// @flow

import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import history from './../../../../navigation/history';
import fetch from './../../../../api';

export function* sendForm(action: any): any {
  try {
    const token = '' + history.location.search.split('=')[1];
    const response = yield call(fetch.put, '/auth/resetPassword', {
      newPassword: action.payload.newPassword,
      token
    });
    
    yield put({
      type: t.RESET_PASSWORD_SUCCEEDED,
      payload: { ...response },
      isSuccess: response.data.success,
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.RESET_PASSWORD_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}


export default function* watcherSendForm(): any {
  yield takeEvery(t.RESET_PASSWORD, sendForm);
}
