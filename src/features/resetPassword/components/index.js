import * as React from "react";
import { Link, Redirect } from "react-router-dom";
import Input from "./../../../components/forms/Input";
import { Form, FormSpy } from 'react-final-form';
import "./style/style.css";
import DefaultBtn from '../../../components/buttons/defaultBtn';
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";

export default (props): React.Node | null => {
  const { errorData, isSuccess, validate, form } = props;
  const { sendForm, updateFormState } = props.actions;
  const { from } = props.location.state || {};

  if (isSuccess) {
    return <Redirect to={from || { pathname: "/auth/signin" }} />;
  }
  return (
    <div className="form_container">
        <div className="logo">
          <Link to={`/auth/start`}><Icon type={i.LOGO} alt="SkillSharing" /></Link>
        </div>
        <div className="resetPassword_form_header">
          reset the password
        </div>
        <div className="form_wrapper">
          <Form
          onSubmit = {sendForm}
          validate={validate}
          >
          {({handleSubmit})=>(
            <form
            onSubmit={handleSubmit}
            className="resetPassword_form_body"
            >
            <FormSpy onChange={state => updateFormState(form, state)} />
            <Input
              name="newPassword"
              placeholder="Введите новый пароль"
              label="password"
              type="password"
            />
            <Input
              name="confirm_newPassword"
              placeholder="Повторите пароль"
              label="repeat the password"
              type="password"
            />
            <div className="btn_wrapper_reset">
              <DefaultBtn type="submit">
                get started!
              </DefaultBtn>
            </div>
            {errorData && <div className="text-danger">{errorData.message}</div>}
          </form>
          )}
          </Form>
          </div>
        </div>
  );
};

