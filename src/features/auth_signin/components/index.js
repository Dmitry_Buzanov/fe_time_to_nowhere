// @flow
import * as React from "react";
import {Link, Redirect} from "react-router-dom";
import {Form, FormSpy} from 'react-final-form'
import "./style/style.css";
import Input from "./../../../components/forms/Input";
import Signwith from './../../../components/signwith/index';
import DefaultBtn from '../../../components/buttons/defaultBtn';
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";

export default (props): React.Node | null => {
    const {errorData, pageData, validate, form} = props;
    const {sendForm, updateFormState} = props.actions;
    const {from} = props.location.state || {};
    if (pageData && pageData.token) {
        return <Redirect to={from || {pathname: "/"}}/>;
    }
    return (
        <div className="root">
            <div className="logo">
                <Link to={`/auth/start`}><Icon type={i.LOGO} alt="SkillSharing"/></Link>
            </div>
            <div className="wrapped-root">
                <div className="linksSign">
                    <Link to={`/auth/signin`} className="signinText">sign in</Link>
                    <Link to={`/auth/signup`} className="signUpLink">sign up</Link>
                </div>
                <Form
                    onSubmit={sendForm}
                    validate={validate}
                    render={({handleSubmit}) => (
                        <form
                            onSubmit={handleSubmit}
                            className="signInForm">
                            <FormSpy onChange={(state) => updateFormState(form, state)}/>
                            <Input
                                name="email"
                                type="text"
                                placeholder="example@gmail.com"
                                label="e-mail"
                            />
                            <Input
                                name="password"
                                type="password"
                                placeholder="Введите пароль"
                                label="password"
                            />
                            <Link to={`/auth/forgot`} className="forgotSignin">forgot your password?</Link>
                            <div className="btnGetStartDiv">
                                <DefaultBtn type="submit">get started!</DefaultBtn>
                            </div>
                            {errorData && <div className="text-danger">{errorData.message}</div>}
                        </form>
                    )}
                />
                <Signwith/>
            </div>
        </div>
    );
};