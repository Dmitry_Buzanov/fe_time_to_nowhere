import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetch from './../../../../api';
import * as contextT from './../../../context/actionTypes'

export function* sendForm(action: any): any {
  try {
    const response = yield call(fetch.post, '/auth/signin', action.payload);
    const jwtAuth =  {
      token: response.data.token,
      refreshToken: response.data.refreshToken
    };
    const responseData = JSON.parse(response.config.data)
    localStorage.setItem("jwtAuth", JSON.stringify(jwtAuth));
    yield put({
      type: contextT.GET_USER_DATA,
      payload: {email:responseData.email}
    });    
    yield put({
      type: contextT.ADD_CONTEXT,
      payload: response.data
    });
    yield put({
      type: t.SIGNIN_SUCCEEDED,
      payload: { ...response, user_id: action.payload.username }
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.SIGNIN_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}

export default function* watcherSendForm(): any {
  yield takeEvery(t.SIGNIN, sendForm);
}