// @flow
import { NAME } from './constants';

export const SIGNIN = `${NAME}/SIGNIN`;
export const SIGNIN_SUCCEEDED = `${NAME}/SIGNIN_SUCCEEDED`;
export const SIGNIN_FAILED = `${NAME}/SIGNIN_FAILED`;
export const UPDATE_FORM_STATE = `final-form-redux-example/finalForm/UPDATE_FORM_STATE`;

export const REFRESH = `${NAME}/REFRESH`;
