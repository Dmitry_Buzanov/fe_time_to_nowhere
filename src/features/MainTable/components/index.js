// @flow
import * as React from "react";
import Table from '../../../components/table/HOC';

export default (props={}): React.Node | null => {
  return (
        <Table data={props.table} type="home" sortColumns={[3,4]} />
  );
};
