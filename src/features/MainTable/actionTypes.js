// @flow
import { NAME } from './constants';

export const LOAD_UP = `${NAME}/LOAD_UP`;
export const LOAD_UP_SUCCEEDED = `${NAME}/LOAD_UP_SUCCEEDED`;
export const LOAD_DATA_SUCCEEDED = `LOAD_DATA_SUCCEEDED`;
export const FETCH_SUCCEEDED = `${NAME}/FETCH_SUCCEEDED`;
export const LOAD_UP_FAILED = `${NAME}/LOAD_UP_FAILED`;

export const REFRESH = `${NAME}/REFRESH`;
