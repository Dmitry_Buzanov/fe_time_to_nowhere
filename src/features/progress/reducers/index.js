import * as t from './../actionTypes';

export const initialState = {
  error: null,
  tasks: [],
  isFetching: false,
};

export default (state=initialState, action) => {
  switch (action.type) {
    case t.FETCH_PROGRESS_REQUEST: {
      return { ...state, isFetching: true, error: null };
    }
    case t.FETCH_PROGRESS_SUCCESS: {
      return { ...state, isFetching: false, tasks: action.payload };
    }
    case t.FETCH_PROGRESS_FAILURE: {
      return { ...state, isFetching: false, error: action.error  };
    }
    default:
      return state;
  }
};