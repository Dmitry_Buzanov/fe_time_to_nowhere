// @flow
import * as t from "./../../actionTypes";
import { call, put, takeEvery } from "redux-saga/effects";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";

const mock = new MockAdapter(axios);

mock.onGet("home/progress").reply(200, {
  tasks: [{
    title: "Javascript",
    tasksCompleted: 12,
    tasksToPromotion: 3,
    remainingTasks: [{
      text: "Прочитать Книгу Java Script начальный курс Frontend разработчика.",
      dateInfo: {
        startDate: "01.10.2019",
        endDate: "01.10.2019"
      }
    },
      {
        text: "Заверстать Landing Page по готовому макету.",
        dateInfo: {
          startDate: "01.10.2019",
          endDate: "01.10.2019"
        }
      },
      {
        text: "Подготовить 4 анимации появления ховеров и поп-апов на HTML5.",
        dateInfo: {
          startDate: "01.10.2019",
          endDate: "01.10.2019"
        }
      }]
  },
    {
      title: "PHP",
      tasksCompleted: 5,
      tasksToPromotion: 10,
      remainingTasks: [{
        text: "Прочитать Книгу Java Script начальный курс Frontend разработчика.",
        dateInfo: {
          startDate: "01.10.2019",
          endDate: "01.10.2019"
        }
      },
        {
          text: " Заверстать Landing Page по готовому макету.",
          dateInfo: {
            startDate: "01.10.2019",
            endDate: "01.10.2019"
          }
        },
        {
          text: "Подготовить 4 анимации появления ховеров и поп-апов на HTML5.",
          dateInfo: {
            startDate: "01.10.2019",
            endDate: "01.10.2019"
          }
        }]
    },
    {
      title: "Java",
      tasksCompleted: 120,
      tasksToPromotion: null,
      remainingTasks: null
    }]
});

export function* getProgress(action) {
  try {
    const response = yield call(axios.get, "home/progress");
    yield put({
      type: t.FETCH_PROGRESS_SUCCESS,
      payload: response.data.tasks
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.FETCH_PROGRESS_FAILURE,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}


export default function* watcherFetchProgress() {
  yield takeEvery(t.FETCH_PROGRESS_REQUEST, getProgress);
}
