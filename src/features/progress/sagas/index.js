// @flow

import { NAME } from './../constants';

import watcherFetchProgress from './getProgress'

export default {
    [`${NAME}-watcherFetchProgress`]: watcherFetchProgress,
};
  