import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  padding: 0 70px 20px 0
  & > section {
    margin-left: 95px;
  }
`;