import React from "react";
import ProgressCard from "../../../components/cards/components/progressCard";
import { Wrapper } from "./styles";


export default (props) => {
  return (
    <Wrapper>
      {props.tasks.map(task => {
        const onePercent = (task.tasksCompleted + task.tasksToPromotion) / 100;
        const percentCompleted = Math.round(task.tasksCompleted / onePercent);
        return (
          <ProgressCard
            task={task}
            percent={percentCompleted}
            diametr={92}
            strokeWidth={5}
            actions={{}}
            small
          />
        );
      })}

    </Wrapper>
  );
}