// @flow

import { createSelector } from 'reselect';
import {NAME} from '../constants'
export const getTasks = (state) => state[NAME].tasks;
export const getStatus = (state) => state[NAME].isFetching;
export const getError = (state) => state[NAME].error;

export const selectStatus = createSelector(getStatus, (isFetching) => {
  return isFetching;
});
export const selectTasks = createSelector(getTasks, (pageData) => {
  return pageData;
});
export const selectError = createSelector(getError, (error) => {
  return error;
});
