import * as t from "./../actionTypes";


const fetchProgressRequest = (id) => ({
  type: t.FETCH_PROGRESS_REQUEST,
  id
});

const fetchProgressSuccess = (payload) => ({
  type: t.FETCH_PROGRESS_SUCCESS,
  payload
});

const fetchProgressFailure = (error) => ({
  type: t.FETCH_PROGRESS_FAILURE,
  error
});

export {
  fetchProgressRequest,
  fetchProgressSuccess,
  fetchProgressFailure
}