// @flow
import * as React from "react";
import Table from '../../../components/table/HOC';

export default (props={}): React.Node | null => {
  
  return (
    <div className="justify-content-center">
      <Table type="mentors" sortColumns={[1,2]}/>
    </div>
  );
};
