import * as t from './../../actionTypes';

export default (data: any) => ({
    type: t.GET_USER_DATA,
    payload: data
});