import * as t from './../actionTypes';

export const initialState: State = {
  error: null,
  contextData: null,
  userData: null,
  isLoad: false
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case t.ADD_CONTEXT: {
      return { ...state, isLoad: false, contextData: action.payload, };
    }
    case t.GET_USER_DATA: {
      return { ...state, isLoad: false, userData: action.payload, };
    }
    case t.DELETE_TOKEN : {
      return { ...state, contextData: null }
    }

    default:
      return state;
  }
};