import { NAME } from './constants';


export const ADD_CONTEXT = `${NAME}/ADD_CONTEXT`;
export const GET_USER_DATA = `${NAME}/GET_USER_DATA`;
export const DELETE_TOKEN = 'DELETE_TOKEN';