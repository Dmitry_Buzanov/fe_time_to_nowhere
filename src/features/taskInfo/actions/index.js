import * as t from "./../actionTypes";


export const toggleTask = (id) => ({
  type: t.TOGGLE_TASK,
  id
});

const fetchTaskRequest = (id) => ({
  type: t.FETCH_TASK_REQUEST,
  id
});

const fetchTaskSuccess = (payload) => ({
  type: t.FETCH_TASK_SUCCESS,
  payload
});

const fetchTaskFailure = (error) => ({
  type: t.FETCH_TASK_FAILURE,
  error
});


export const
