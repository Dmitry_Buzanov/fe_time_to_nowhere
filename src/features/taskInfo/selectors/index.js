// @flow

import { createSelector } from 'reselect';
import {NAME} from './../constants'

export const getTask = (state) => state[NAME].task;
export const getStatus = (state) => state[NAME].isFetching;
export const getError = (state) => state[NAME].error;
export const getPopupStatus = (state) => state[NAME].popupIsOpen;


export const selectSpheres = createSelector(getSpheres, (pageData) => {
  return pageData;
});
export const selectError = createSelector(getError, (error) => {
  return error;
});

export const selectPopupStatus = createSelector(getPopupStatus, (popupIsOpen) => {
  return popupIsOpen;
});
