import styled from "styled-components";

export const Wrapper = styled.div`
  margin-left: 95px;
  position: relative;
  & > span {
    position: absolute;
    top: 12px;
    right: 80px;
    display: flex;
    font-family: Muli;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 1;  
  }
`;
export const TaskTitle = styled.h2`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: normal;
  color: #393939;
  margin-top: 42px;
  margin-bottom: 68px;
`;

export const ContentWrapper = styled.div`
  display: flex;
  width: 1260px;
`;

export const ProgressBar = styled.div`
  width: 246px;
  height: 246px;
  position: relative;
  margin-bottom: 20px
  &:first-child {
    margin-right: 50px;
  }
  & > div:nth-child(2)  {
    margin-bottom: 20px;
  }
`;

export const Item = styled.div`
  display: flex;
  margin-bottom: 30px
`;

export const Title = styled.h3`
    font-family: Muli;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: normal;
    color: #393939 
    margin-top: 0;
    margin-bottom: 0;
    margin-right: 40px;
    `;
export const Text = styled.div`
  font-family: Muli;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 2.4;
  color: #393939;
`;

export const SphereStatistick = styled.div`
    width: 732px;
    height: 399px;
    display:flex; 
`;
export const TasksInfo = styled.div`
    width: 702px;
    height: 313px;
    margin-right: 100px
`;

export const TaskDescription = styled.div`
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: normal;
    margin-bottom: 45px;
`;
export const TaskList = styled.ul`
  list-style-type: none;
  &:last-child label:before {
    margin: 0;
  }
`;
export const TaskListItem = styled.div`
  position: relative;
  margin-left: 49px;
  margin-bottom: 27px;
  & > div:first-child {
    left: -90px;
    top: 0;
  }
`;