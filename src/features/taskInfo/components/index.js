import React from "react";
import Checkbox from "./../../../components/forms/Checkbox";
import Diagram from '../../../components/diagram/components/';
import Status from './../../../components/table/status/'

import {
  TaskTitle,
  ContentWrapper,
  ProgressBar,
  Item,
  Title,
  Text,
  SphereStatistick,
  TasksInfo,
  TaskDescription,
  TaskList,
  Wrapper,
  TaskListItem
} from "./styles/";

export default props => {
  return (<Wrapper>
      <TaskTitle>
        Создать макеты интернет-магазина в Figma
      </TaskTitle>
      <span>in progress <Status text="in process"/></span>
      <ContentWrapper>
        <TasksInfo>
          <TaskDescription>
            Тебе нужно разаработать “под ключ” дизайн макеты интернет магазаина для техники Apple. Для начала сойдет черновая концепция сайта. По типографике можешь не сильно загоняться- все равно будем еще смотреть м править. У тебя месяц.
          </TaskDescription>
          <TaskList>
           <TaskListItem>
              <Checkbox isSelected onClick={() => {}} id={1}/>
              <div>Создать тех карту сайта </div>
            </TaskListItem>
            <TaskListItem>
              <Checkbox isSelected onClick={() => {}} id={2}/>
              <div>Сделать wireframe прототипы </div>
            </TaskListItem>
            <TaskListItem>
              <Checkbox isSelected onClick={() => {}} id={3} />
              <div>Разработать UI концепт сайта</div>
            </TaskListItem>
            <TaskListItem>
              <Checkbox isSelected={true} onClick={() => {}} id={4}/>
              <div>Сделать иконки для сайта</div>
            </TaskListItem>
            <TaskListItem>
              <Checkbox isSelected={true} onClick={() => {}} id={5} />
              <div>Сделать UI-kit</div>
            </TaskListItem>
          </TaskList>
        </TasksInfo>
        <SphereStatistick>
          <ProgressBar>
            <Item>
              <Title>Sphere: </Title><Text>UX/UI Design</Text>
            </Item>
            <Diagram
              percent={50}
              diametr={230}
              strokeWidth={15}
              actions={{}}
            />
            <Item>
              <Title>Start date: </Title><Text>01.10.2019</Text>
            </Item>
          </ProgressBar>
          <ProgressBar>
            <Item>
              <Title>Mentor: </Title><Text>Andrew Dostanko</Text>
            </Item>
            <Diagram
              data={{}}
              day={2}
              days={0}
              diametr={230}
              strokeWidth={15}
              actions={{}}
            />
            <Item>
              <Title>Deadline: </Title><Text>01.11.2019</Text>
            </Item>
          </ProgressBar>
        </SphereStatistick>
      </ContentWrapper>
    </Wrapper>
  );
};