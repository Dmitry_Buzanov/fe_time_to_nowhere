// // @flow
//
// import * as actionType from "./../actionTypes";
// import { call, put, takeEvery } from "redux-saga/effects";
// import fetch from "../../../api/index"
// import MockAdapter from "axios-mock-adapter";
//
// const mock = new MockAdapter(fetch);
// mock.onGet("home/task").reply(200, {
//
// })
// export function* loadTask(action: any): any {
//   try {
//     const id = action.id;
//     const url =`http://localhost:9003/api/v2/spheres/user/${id}/list`;
//     const response = yield call(fetch.get, "home/task");
//     yield put({
//       type: actionType.FETCH_TASK_SUCCESS,
//       payload: response.data
//     });
//   } catch (error) {
//     yield put({
//       type: actionType.FETCH_TASK_FAILURE,
//       error
//     });
//   }
// }
//
// export default function* watcherFetchSpheres(): any {
//   yield takeEvery(actionType.FETCH_TASK_REQUEST, loadTask);
// }