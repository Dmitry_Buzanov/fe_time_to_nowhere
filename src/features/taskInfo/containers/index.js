// // @flow
//
// import { connect } from 'react-redux';
// import * as actionCreators from './../actions/index';
// import { bindActionCreators } from 'redux';
// import {
//   compose,
//   setDisplayName,
//   lifecycle
// } from "recompose";
// import addSaga from "./../../../HOComponents/addSaga";
// import addReducer from "./../../../HOComponents/addReducer";
// import Component from "./../components";
// import reducers from "./../reducers";
// import sagas from "./../sagas";
// import {
//   selectSpheres,
//   selectError,
//   selectStatus,
//   selectPopupStatus,
//   selectUserId
// } from './../selectors/index';
// import {NAME} from "../constants"
//
// const mapStateToProps = (state) => ({
//   error: selectError(state),
//   spheres: selectSpheres(state),
//   isFetching: selectStatus(state),
//   popupIsOpen: selectPopupStatus(state),
//   id: selectUserId(state),
// });
//
// const mapDispatchToProps = (dispatch: any) => ({
//   actions: bindActionCreators(actionCreators, dispatch)
// });
//
// export default compose(
//   connect(mapStateToProps, mapDispatchToProps),
//   addReducer(NAME, reducers),
//   addSaga({sagas}),
//   setDisplayName(`components/${NAME}`),
//   lifecycle({
//     componentDidMount(){
//       this.props.actions.fetchSphereRequest(this.props.id);
//     }
//   })
// )(Component);
//
