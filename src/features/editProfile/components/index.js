// @flow
import * as React from "react";
import SideMenu from '../../../components/menus/SideMenu';
import EditForm from '../../../components/forms/EditForm/index';

export default (): React.Node | null => {
    const date = {
        day: '21',
        month: '02',
        year: '2019'
    };
  return (
    <div className="justify-content-center">

      <SideMenu />
      <EditForm date={date} position="Senior PHP Developer" mail="banana@gmail.com" image="http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg" name="Vlad" surename='Rusakevich' gender="male"/>
    </div>
  );
};
