import { createSelector } from 'reselect';
import { NAME } from './../constants';

export const getPageData = (state: any) => state[NAME].pageData;
export const getLoader = (state: any) => state[NAME].isLoad;
export const getError = (state: any) => state[NAME].error;
export const getFlag = (state: any) => state[NAME].isClosed;

export const selectLoader = createSelector(getLoader, (isLoad: any) => {
  return isLoad;
});

export const selectPageData = createSelector(getPageData, (pageData: any) => {
  return pageData;
});

export const selectFlag= createSelector(getFlag, (isClosed: any) => {
  return isClosed;
});

export const selectError = createSelector(getError, (error: any) => {
  return error;
});
