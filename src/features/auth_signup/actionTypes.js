import { NAME } from './constants';

export const SIGNUP = `${NAME}/SIGNUP`;
export const SIGNUP_SUCCEEDED = `${NAME}/SIGNUP_SUCCEEDED`;
export const SIGNUP_FAILED = `${NAME}/SIGNUP_FAILED`;
export const UPDATE_FORM_STATE = 'final-form-redux-example/finalForm/UPDATE_FORM_STATE'
export const REFRESH = `${NAME}/REFRESH`;
export const CLOSE_POPUP = `${NAME}/CLOSE_POPUP`;