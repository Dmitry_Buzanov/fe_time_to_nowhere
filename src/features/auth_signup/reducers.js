import * as t from './actionTypes';
import type { State } from './types';

export const initialState: State = {
  error: null,
  pageData: null,
  isLoad: false,
  isClosed: true
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case t.UPDATE_FORM_STATE:
    return {
      ...state,
      [action.form]: action.payload
    }
    case t.SIGNUP: {
      return { ...state, isLoad: true, error: null };
    }
    case t.SIGNUP_FAILED: {
      return { ...state, isLoad: false, error: action.error };
    }
    case t.SIGNUP_SUCCEEDED: {
      return { ...state, isLoad: false, pageData: action.payload, isClosed: false };
    }
    case t.CLOSE_POPUP: {
      return{...state, isClosed: true }
    }
    case t.REFRESH: {
      return initialState;
    }
    default:
      return state;
  }
};