import * as t from "./actionTypes";

export const sendForm = (data: any = {}) => ({
  type: t.SIGNUP,
  payload: data
});

export const refresh = () => ({
  type: t.REFRESH
});

export const closePopup = () => ({
  type: t.CLOSE_POPUP
});

export const updateFormState = (form, state) => ({
  type: t.UPDATE_FORM_STATE,
  form,
  payload: state
});
