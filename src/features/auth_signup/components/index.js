import * as React from "react";
import { Link, Redirect } from "react-router-dom";
import { Form } from "react-final-form";
import Input from "../../../components/forms/Input";
import Popup from "./../../../components/popup";
import { FormSpy } from "react-final-form";
import "./style/style.css";
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";
import Signwith from "../../../components/signwith";
import DefaultBtn from "../../../components/buttons/defaultBtn";

export default (props): React.Node | null => {
  const { errorData, pageData, validate, form } = props;
  const { sendForm, updateFormState } = props.actions;
  const { from } = props.location.state || {};
  if (pageData && pageData.data.success && props.isClosed) {
    return <Redirect to={from || { pathname: "/" }} />;
  }
  return (
    <div className="form_container">
      <div className="logo">
        <Link to={`/auth/start`}>
          <Icon type={i.LOGO} alt="SkillSharing" />
        </Link>
      </div>
      <div className="form_wrapper">
        <div className="register_form_header">
          <Link className="active" to={`/auth/signup`}>
            sign up
          </Link>
          <Link to={`/auth/signin`}>sign in</Link>
        </div>
        <Form onSubmit={sendForm} validate={validate}>
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit} className="register_form_body">
              <FormSpy onChange={state => updateFormState(form, state)} />
              <Input
                name="firstName"
                type="text"
                label="firstname"
                placeholder="firstname"
              />
              <Input
                name="lastName"
                type="text"
                label="lastname"
                placeholder="lastname"
              />
              <Input
                name="email"
                type="text"
                label="e-mail"
                placeholder="e-mail"
              />
              <Input
                name="password"
                placeholder="password"
                label="password"
                type="password"
              />
              <Input
                name="confirm_password"
                placeholder="confirm password"
                label="repeat the password"
                type="password"
              />
              <div className="btn_wrapper">
                <DefaultBtn type="submit">get started!</DefaultBtn>
              </div>
              {errorData && (
                <div className="text-danger">{errorData.message}</div>
              )}
              {!props.isClosed &&
                props.pageData &&
                !props.pageData.data.isConfirmed && (
                  <Popup
                    title="ConfirmEmail"
                    message="Please, go to your Email and confirm your account."
                    onClick={props.actions.closePopup}
                  />
                )}
            </form>
          )}
        </Form>
        <Signwith />
      </div>
    </div>
  );
};
