// @flow
import * as React from "react";
import SideMenu from '../../../components/menus/SideMenu';
import MainMenu from '../../../components/mainMenu';
import taskInfo from '../../../features/taskInfo/components';
import Header from '../../../components/header';
import PrivateRoute from "./../../../components/routers/PrivateRoute";
import {Switch} from 'react-router'
import {ContentWrapper} from './style/index'
import Mentors from "../../mentors/index";
import Tasks from "../../tasks";
import Students from "../../students";
import mySpheres from "../../mySpheres/containers/"
import MainTable from "../../MainTable/index"
import Progress from "../../progress/containers/"
import Learning from "../../learning"
import AddTask from "../../addTask"

export default (props={}): React.Node | null => {
  return (
    <div className="justify-content-center">
      <Header title="My profile" name="Michael Hafner" date="2 March, 1991" position="Senior PHP Developer" image="http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg" />
      <MainMenu />
      <SideMenu />
      <ContentWrapper>
        <Switch>
          <PrivateRoute path='/home/knowledge' component={mySpheres} />
          <PrivateRoute path='/home/tasks' component={Tasks} /> 
          <PrivateRoute path="/home/students" component={Students} />
          <PrivateRoute path="/home/mentors" component={Mentors} />
          <PrivateRoute path="/home/progress" component={Progress} />
          <PrivateRoute exact path="/home" component={MainTable} />
          <PrivateRoute exact path="/home/task" component={taskInfo} />
          <PrivateRoute exact path="/learning" component={Learning} />
          <PrivateRoute exact path="/addTask" component={AddTask} />
        </Switch>
      </ContentWrapper>
    </div>
  );
};
