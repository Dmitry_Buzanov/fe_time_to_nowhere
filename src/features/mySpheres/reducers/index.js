import * as t from './../actionTypes';

export const initialState = {
  error: null,
  spheres: [],
  isFetching: false,
  popupIsOpen: false
};

export default (state=initialState, action) => {
  switch (action.type) {
    case t.FETCH_SPHERES_REQUEST: {
      return { ...state, isFetching: true, error: null };
    }
    case t.FETCH_SPHERES_FAILURE: {
      return { ...state, isFetching: false, error: action.error };
    }
    case t.FETCH_SPHERES_SUCCESS: {
      return { ...state, isFetching: false, spheres: action.payload };
    }
    case t.ADD_SPHERE: {
      return { ...state, spheres: [...state.spheres, action.payload]};
    }
    case t.DELETE_SPHERE: {
      return { ...state, spheres: state.spheres.filter(sphere => sphere.id !== action.id)};
    }
    case t.OPEN_POPUP: {

      return { ...state, popupIsOpen: true };
    }
    case t.CLOSE_POPUP: {
      return { ...state, popupIsOpen: false};
    }

    default:
      return state;
  }
};