import styled from "styled-components";

export const Title = styled.h1`
  margin-left: 90px;
  margin-bottom: 30px; 
`;

export const SphereWrapper = styled.div`
   display: flex;
   flex-direction: column;
  justify-content: center;
  flex-wrap: wrap;
  padding: 60px 21px 21px 70px;
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  flex-wrap: wrap;
  margin-right: 0px;
    & > div {
    margin-right: 40px
  }
`;