// @flow
import React, { Fragment } from "react";
import SphereCard from "../../../components/cards/components/sphereCard/index";
import AddSphereCard from "../../../components/cards/components/addSphereCard/index";
import AddSpherePopup  from "../../../components/cards/components/addSphereCard/components/popup";
import { SphereWrapper, Wrapper, Title } from "./styles";


export default (props = {}): React.Node | null => {
  const deleteSphere = (id, userid) => {
    return () => { props.actions.deleteSphere(id, userid) };
  };
  return (
    <Fragment>
      {props.popupIsOpen && <AddSpherePopup onClick={props.actions.closePopup} /> }
      <SphereWrapper>
      <Title>My Spheres</Title>
        <Wrapper>
          {!props.isFetching && props.spheres.map((sphere,index) => {
            return (<SphereCard onClick={deleteSphere(sphere.id, props.id)} {...sphere}/>);
          })}
        <AddSphereCard onClick={props.actions.openPopup}>Add new knowledge sphere</AddSphereCard>
        </Wrapper>
      </SphereWrapper>
    </Fragment>

  );
};
