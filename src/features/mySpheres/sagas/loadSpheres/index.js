// @flow

import * as actionType from "../../actionTypes";
import { call, put, takeEvery } from "redux-saga/effects";

import MockAdapter from "axios-mock-adapter";
import axios from 'axios';

const data = [
  {
    id:1,
    status: "Student",
    name: "Design",
    level: "Junior",
    tasksDone: 5,
    tasksCreated: 0,
    students: 0
  }, {
    id:2,
    status: "Mentor",
    name: "Java Script",
    level: "Senior",
    tasksDone: 52,
    tasksCreated: 14,
    students: 4
  }, {
    id:3,
    status: "Student",
    name: "Design",
    level: "Junior",
    tasksDone: 5,
    tasksCreated: 0,
    students: 0
  }
];

const mock = new MockAdapter(axios);

mock.onGet("home/knowledge").reply(200, data);

export function* loadSpheres(action: any): any {
  try {
    // const id = action.id;
    // const url =`http://localhost:9003/api/v2/spheres/user/${id}/list`;
    const response = yield call(axios.get, "home/knowledge");
    yield put({
      type: actionType.FETCH_SPHERES_SUCCESS,
      payload: response.data
    });
  } catch (error) {
    yield put({
      type: actionType.FETCH_SPHERES_FAILURE,
      error
    });
  }
}

export default function* watcherFetchSpheres(): any {
  yield takeEvery(actionType.FETCH_SPHERES_REQUEST, loadSpheres);
}