// @flow

import * as actionType from "../../actionTypes";
import { put, takeLatest } from "redux-saga/effects";

export function* deleteSpheres(action: any): any {
  try {
    // const id = action.userid;
    // const url =`http://localhost:9003/api/v2/spheres/user/${id}/`;
    // const response = yield call(fetch.delete, url);
    yield put({
      type: actionType.DELETE_SPHERE,
    });
  } catch (error) {
    console.error(error)
  }
}

export default function* watcherFetchSpheres(): any {
  yield takeLatest(actionType.DELETE_SPHERE, deleteSpheres);
}