// @flow

import watcherFetchSpheres from './loadSpheres';
import watcherDeleteSPheres from './deleteSpheres'
export default {
  [`watcherFetchSpheres`]: watcherFetchSpheres,
  [`watcherDeleteSPheres`]: watcherDeleteSPheres
};
