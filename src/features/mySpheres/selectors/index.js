// @flow

import { createSelector } from 'reselect';
const spherePage = "mySpheres";

export const getSpheres = (state) => state[spherePage].spheres;
export const getStatus = (state) => state[spherePage].isFetching;
export const getError = (state) => state[spherePage].error;
export const getPopupStatus = (state) => state[spherePage].popupIsOpen;
export const getUserId = (state) => '5c9b680be35c0026ec942214';


export const selectStatus = createSelector(getStatus, (isFetching) => {
  return isFetching;
});

export const selectSpheres = createSelector(getSpheres, (pageData) => {
  return pageData;
});
export const selectError = createSelector(getError, (error) => {
  return error;
});

export const selectPopupStatus = createSelector(getPopupStatus, (popupIsOpen) => {
  return popupIsOpen;
});

export const selectUserId = createSelector(getUserId, id => id)