import * as t from "./../actionTypes";


const fetchSphereRequest = (id) => ({
  type: t.FETCH_SPHERES_REQUEST,
  id
});

const fetchSphereSuccess = (payload) => ({
  type: t.FETCH_SPHERES_SUCCESS,
  payload
});

const fetchSphereFailure = (error) => ({
  type: t.FETCH_SPHERES_FAILURE,
  error
});

const addSphere = (id) => ({
  type: t.ADD_SPHERE,
  payload: id
});

const deleteSphere = (id, userid) => ({
  type: t.DELETE_SPHERE,
  id,
  userid
});

const openPopup = () => ({
  type: t.OPEN_POPUP
});

const closePopup = () => ({
  type: t.CLOSE_POPUP
})

export {
  fetchSphereRequest,
  fetchSphereSuccess,
  fetchSphereFailure,
  addSphere,
  deleteSphere,
  openPopup,
  closePopup
}