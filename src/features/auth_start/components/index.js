// @flow
import * as React from "react";
import { Link } from 'react-router-dom';
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";
import background from './style/assets/Rectangle.png';
import Signwith from "../../../components/signwith";
import DefaultBtn from "../../../components/buttons/defaultBtn"
import "./style/style.css";

export default (props): React.Node | null => {
  return (
    <div className="wrapper">
      <div className="bg-wrapper">
        <img src={background} alt="SkillSharing"></img>
      </div>
      <div className="container">

            <div className="main-logo">
              <Icon type={i.LOGO} alt="SkillSharing" />
            </div> 

            <div className="slogan">
              Improve your skills easy, and share them with others!
            </div>
            
            <div className="start-navigation">
                <Link to={`/auth/signin`} className="buttonWrapper">
                  <DefaultBtn>
                    signin
                  </DefaultBtn>
                </Link>
                <Link to={`/auth/signup`} className="buttonWrapper">
                  <DefaultBtn>
                    signup
                  </DefaultBtn>
                </Link>
            </div>
            <Signwith/>
      </div>
    </div>
  );
};