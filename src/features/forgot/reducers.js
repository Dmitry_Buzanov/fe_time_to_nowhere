// @flow
import * as t from './actionTypes';
import type { State } from './types';

export const initialState: State = {
  error: null,
  isOpenModal: null,
  pageData: null,
  isLoad: false,
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case t.UPDATE_FORM_STATE: {
      return { ...state, [action.form]: action.payload };
    }
    case t.FORGOT_PASSWORD: {
      return { ...state, isLoad: true, error: null };
    }
    case t.FORGOT_PASSWORD_FAILED: {
      return { ...state, isLoad: false, error: action.error };
    }
    case t.FORGOT_PASSWORD_SUCCEEDED: {
      return { ...state, isLoad: false, pageData: action.payload, isOpenModal: action.isOpenModal };
    }
    case t.CLOSE_POPUP: {
      return{...state, isOpenModal: false }
    }

    case t.REFRESH: {
      return initialState;
    }

    default:
      return state;
  }
};