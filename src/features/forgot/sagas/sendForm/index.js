// @flow

import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetch from './../../../../api';

export function* sendForm(action: any): any {
  try {
    const response = yield call(fetch.put, '/auth/forgot', {
      email: action.payload.email
    });
    
    yield put({
      type: t.FORGOT_PASSWORD_SUCCEEDED,
      payload: { ...response, user_id: action.payload.email },
      isOpenModal: response.data.success,
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.FORGOT_PASSWORD_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}


export default function* watcherSendForm(): any {
  yield takeEvery(t.FORGOT_PASSWORD, sendForm);
}
