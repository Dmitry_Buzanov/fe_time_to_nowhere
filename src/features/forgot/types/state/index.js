// @flow
export default type State = {
    isLoad: boolean,
    error: any,
    pageData: any,
    isModalOpen: any,
};