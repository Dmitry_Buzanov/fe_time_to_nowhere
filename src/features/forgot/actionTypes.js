// @flow
import { NAME } from './constants';

export const FORGOT_PASSWORD = `${NAME}/FORGOT_PASSWORD`;
export const FORGOT_PASSWORD_SUCCEEDED = `${NAME}/FORGOT_PASSWORD_SUCCEEDED`;
export const FORGOT_PASSWORD_FAILED = `${NAME}/FORGOT_PASSWORD_FAILED`;

export const UPDATE_FORM_STATE = 'final-form-redux-example/finalForm/UPDATE_FORM_STATE'
export const REFRESH = `${NAME}/REFRESH`;
export const CLOSE_POPUP = `${NAME}/CLOSE_POPUP`;
