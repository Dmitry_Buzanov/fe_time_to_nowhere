import * as React from "react";
import { Form, FormSpy } from 'react-final-form';
import { Link, Redirect } from "react-router-dom";

import Input from "./../../../components/forms/Input";
import Popup from "./../../../components/popup";
import DefaultBtn from "../../../components/buttons/defaultBtn";
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";

import "./style/style.css";


export default (props): React.Node | null => {
  const { errorData, isOpenModal, pageData, validate, form } = props;
  const { sendForm, updateFormState } = props.actions;
  const { from } = props.location.state || {};
  
  if (pageData && pageData.token) {
    return <Redirect to={from || { pathname: "/" }} />;
  }
  return (
    <div className="form_container">
        <div className="logo">
          <Link to={`/auth/start`}><Icon type={i.LOGO} alt="SkillSharing" /></Link>
        </div>
        <div className="forgot_form_header">
          recover the password
        </div>
        <div className="form_wrapper">
          <div className="forgot_form_text">
            Type your e-mail over here
            to recover your password
          </div>
          <Form
          onSubmit = {sendForm}
          validate={validate}
          >
          {({handleSubmit})=>(
            <form
            onSubmit={handleSubmit}
            className="forgot_form_body"
            >
            <FormSpy onChange={state => updateFormState(form, state)} />
            <Input
              name="email"
              type="text"
              label="e-mail"
              placeholder="Введите ваш email"
            />
            <div className="btn_wrapper_forgot">
              <DefaultBtn type="submit">
                send
              </DefaultBtn>
            </div>
            {errorData && <div className="text-danger">{errorData.message}</div>}
            {isOpenModal && <Popup title = "ResetPassword" message = "Please, go to your Email and reset your password." onClick = {props.actions.closePopup}/>}
          </form>
          )}
          </Form>
        </div>
      </div>
  );
};
