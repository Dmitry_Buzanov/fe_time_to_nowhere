// @flow

import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetch from './../../../../api';
import {REMOVE_CURRENT_TASK} from "../../../../components/table/constants";

export function* loadPage(action: any): any {
  try {
    const response = yield call(fetch.get, 'http://localhost:9003/api/v2/tasks/performer/5ca1d50e1f5a4a23e0440745/tablemain', {
      auth: action.payload
    });
    yield put({
      type: t.LOAD_DATA_SUCCEEDED,
      payload: { ...response.data.tasks}
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.LOAD_UP_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}
export function* removeTask(action: any): any {
  try {
    const url = 'http://localhost:9003/api/v2/task/' + action.id;
    yield call(fetch.delete, url, {
      auth: action.payload
    });
    yield put();
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.LOAD_UP_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}

export default function* watcherloadPage(): any {
  yield takeEvery(t.LOAD_UP, loadPage);
  yield takeEvery(REMOVE_CURRENT_TASK, removeTask);
}