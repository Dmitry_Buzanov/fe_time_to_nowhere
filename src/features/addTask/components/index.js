// @flow
import * as React from "react";
import {ContentWrapper} from './style/index'
import AddTaskForm from '../../../components/forms/AddTaskForm';

export default (props={}): React.Node | null => {
  return (
        <ContentWrapper>
          <h1>Создать макеты интернет-магазина в Figma</h1>
          <AddTaskForm>

          </AddTaskForm>
        </ContentWrapper>
  );
};
