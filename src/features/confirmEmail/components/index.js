// @flow
import * as React from "react";
import { Redirect } from "react-router-dom";


export default (props): React.Node | null => {
  const { from } = props.location.state || {};
  const {confirmEmail} = props.pageData;
  
  if (confirmEmail) { 
    return(<h1>Your Email is confirmed</h1>, <Redirect to={from || { pathname: "/" }} />)
  }
return(<h1>Confirm your Email</h1>)
  
};
