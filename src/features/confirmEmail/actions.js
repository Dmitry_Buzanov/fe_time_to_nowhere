// @flow

import * as t from './actionTypes';
export const sendForm = (data: any = {}) => ({
    type: t.SEND_DATA,
    payload: data
});
  
export const refresh = () => ({
    type: t.REFRESH
});