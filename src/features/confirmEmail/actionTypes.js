// @flow
import { NAME } from './constants';

export const ADD_DATA_FAILED = `${NAME}/ADD_DATA_FAILED`;
export const GET_TOKEN =  `${NAME}/GET_TOKEN`;
export const SEND_DATA = `${NAME}/SEND_DATA`;
export const REFRESH = `${NAME}/REFRESH`;
