import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import history from './../../../../navigation/history'
import fetch from './../../../../api';
export function* sendForm(action: any): any {
  try {
    const token = ""+ history.location.search.split("=")[1];
    const response = yield call(fetch.post, '/auth/confirmemail',{token});
    yield put({
      type: t.GET_TOKEN,
      payload: response.data
    });

  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.ADD_DATA_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}

export default function* watcherSendForm(): any {
  yield takeEvery(t.SEND_DATA, sendForm);
}