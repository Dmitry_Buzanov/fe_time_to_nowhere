// @flow
import * as t from './actionTypes';
import type { State } from './types';

export const initialState: State = {
  error: null,
  pageData: {isConfirmed: null},
  userData: null,
  isLoad: false
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case t.SEND_DATA: {
      return { ...state, isLoad: true, error: null,};
    }
    case t.ADD_DATA_FAILED: {
      return { ...state, isLoad: false, error: action.error };
    }
    case t.GET_TOKEN: {
      return { ...state, isLoad: false, error: null,pageData: action.payload};
    }
    case t.REFRESH: {
      return initialState;
    }

    default:
      return state;
  }
};