// @flow

import axios from "axios";
import faker from "faker";
import history from "./../navigation/history";
import MockAdapter from "axios-mock-adapter";

let baseURL =
  process.env.REACT_APP_SERVER_ADDR || process.env.REACT_APP_MOCK_DOMAIN_NAME;

if (process.env.NODE_ENV === "production") {
  baseURL = "/";
}

const fetch = axios.create({ baseURL });

fetch.interceptors.request.use(
  config => {
    const auth =
      JSON.parse(window.localStorage.getItem("jwtAuth") || "{}") || {};
    if (typeof auth === "object" && auth.token) {
      config.headers.Authorization = "Bearer " + auth.token;
    }
    config.headers["Cache-Control"] =
      "no-cache,no-store,must-revalidate,max-age=-1,private";
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

fetch.interceptors.response.use(
  response => {
    if (
      response.data == null &&
      response.config.responseType === "json" &&
      response.request.responseText != null
    ) {
      try {
        response.data = JSON.parse(response.request.responseText);
      } catch (error) {
        // ignored
      }
    }
    return response;
  },
  error => {
    if (error.response && error.response.status === 401) {
      if (!history.location.pathname.includes("auth")) {
        history.push("/auth/signout");
      }
    }
    return Promise.reject(error);
  }
);

const mock = new MockAdapter(fetch);

mock.onGet("/home").reply(200, {
  tasksTitles: ["Task", "Sphere", "Mentor", "Start date", "Deadline", "Status"],
  tasksRows: [
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "finished",
      id: "0",
      isSelected: false,
      sort: false
    },
    {
      task: "Read many books",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "unfinished",
      id: "1",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "in process",
      id: "2",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "finished",
      id: "3",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "in process",
      id: "4",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "unfinished",
      id: "5",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "unfinished",
      id: "6",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "finished",
      id: "7",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2019",
      deadline: "01.11.2020",
      status: "in process",
      id: "8",
      isSelected: false,
      sort: false
    },
    {
      task: "Read book",
      sphere: "javaScript",
      mentor: "Anton Filin",
      start_date: "01.10.2018",
      deadline: "01.11.2019",
      status: "finished",
      id: "9",
      isSelected: false,
      sort: false
    }
  ]
});

mock.onGet("/mentors").reply(200, {
  tasksTitles: ["Mentor", "Spheres", "Number of students"],
  tasksRows: [
    {
      mentor: "Andrew Dostanko",
      spheres: "javaScript",
      number_of_students: "22 students",
      id: "1",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      mentor: "Andrew Dostanko",
      spheres: "javaScript",
      number_of_students: "12 students",
      id: "2",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      mentor: "Anton Filin",
      spheres: "javaScript",
      number_of_students: "12 students",
      id: "3",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      mentor: "Anton Filin",
      spheres: "javaScript",
      number_of_students: "5 students",
      id: "4",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      mentor: "Andrew Dostanko",
      spheres: "UX/UI Design; Motion Design; GUI Design",
      number_of_students: "12 students",
      id: "5",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      mentor: "Anton Filin",
      spheres: "javaScript",
      number_of_students: "12 students",
      id: "6",
      isSelected: false,
      img: faker.image.avatar()
    }
  ]
});

mock.onGet("/tasks").reply(200, {
  tasksTitles: ["Name of the Sphere", "Name of the task", "Creator"],
  tasksRows: [
    {
      name_of_the_sphere: "Java Script",
      name_of_the_task: "assemble front and back",
      creator: "me",
      id: "1",
      isSelected: false,
      img: faker.image.avatar()
    }
  ]
});
mock.onGet("/students").reply(200, {
  tasksTitles: ["Student", "Spheres", "Number of tasks"],
  tasksRows: [
    {
      spheres: "Motion Design",
      number_of_tasks: "12 tasks",
      student: "Mark Iplier",
      id: "1",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      spheres: "Motion Design",
      number_of_tasks: "12 tasks",
      student: "Mark Iplier",
      id: "2",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      spheres: "Motion Design",
      number_of_tasks: "12 tasks",
      student: "Mark Iplier",
      id: "3",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      spheres: "Motion Design",
      number_of_tasks: "12 tasks",
      student: "Mark Iplier",
      id: "4",
      isSelected: false,
      img: faker.image.avatar()
    },
    {
      spheres: "Motion Design",
      number_of_tasks: "12 tasks",
      student: "Mark Iplier",
      id: "5",
      isSelected: false,
      img: faker.image.avatar()
    }
  ]
});
mock.onGet("/learning").reply(200, {
  tasksTitles: [
    "Name of the Sphere",
    "Number of members",
    "You role in sphere"
  ],
  tasksRows: [
    {
      name_of_the_sphere: "JS",
      number_of_members: "24 members",
      you_role_in_sphere: "Mentor",
      id: "1",
      img: faker.image.avatar()
    },
    {
      name_of_the_sphere: "Java",
      number_of_members: "24 members",
      you_role_in_sphere: "Mentor",
      id: "2",
      img: faker.image.avatar()
    }
  ]
});

mock.onGet("/home/mentors").reply(200, {
  tasks: {
    tasksTitles: ["Name of the Sphere", "Name of the task", "Creator"],
    tasksRows: [
      {
        name_of_the_sphere: "Java Script",
        name_of_the_task: "12",
        creator: "me",
        id: "1",
        isSelected: false,
        img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
      }
    ]
  },
  students: {
    tasksTitles: ["Student", "Spheres", "Number of tasks"],
    tasksRows: [
      {
        spheres: "Motion Design",
        number_of_tasks: "12 tasks",
        student: "Mark Iplier",
        id: "1",
        isSelected: false,
        img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
      },
      {
        spheres: "Motion Design",
        number_of_tasks: "12 tasks",
        student: "Mark Iplier",
        id: "2",
        isSelected: false,
        img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
      },
      {
        spheres: "Motion Design",
        number_of_tasks: "12 tasks",
        student: "Mark Iplier",
        id: "3",
        isSelected: false,
        img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
      },
      {
        spheres: "Motion Design",
        number_of_tasks: "12 tasks",
        student: "Mark Iplier",
        id: "4",
        isSelected: false,
        img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
      },
      {
        spheres: "Motion Design",
        number_of_tasks: "12 tasks",
        student: "Mark Iplier",
        id: "5",
        isSelected: false,
        img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
      }
    ]
  },
  home: {
    tasksTitles: [
      "Task",
      "Sphere",
      "Mentor",
      "Start date",
      "Deadline",
      "Status"
    ],
    tasksRows: [
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "finished",
        id: "0",
        isSelected: false
      },
      {
        task: "Read many books",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "unfinished",
        id: "1",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "in process",
        id: "2",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "finished",
        id: "3",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "in process",
        id: "4",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "unfinished",
        id: "5",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "unfinished",
        id: "6",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "finished",
        id: "7",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2019",
        deadline: "01.11.2020",
        status: "in process",
        id: "8",
        isSelected: false
      },
      {
        task: "Read book",
        sphere: "javaScript",
        mentor: "Anton Filin",
        start_date: "01.10.2018",
        deadline: "01.11.2019",
        status: "finished",
        id: "9",
        isSelected: false
      }
    ]
  }
});
mock.onAny().passThrough();

export default fetch;
