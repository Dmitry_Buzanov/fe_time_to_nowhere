import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  
  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    font-size: 19px;
}

a {
    text-decoration: none;
}

button{
    display: flex;
    cursor: pointer;
    margin: 0;
    padding: 0;
    border: none;
    background: transparent;
}
button:focus{
    outline: none;
}
}
`