// @flow
import * as t from './actionTypes';
import type { State } from './types';

export const initialState: State = {
  error: null,
  pageData: null,
  isLoad: false,
  percent: 0,
  day:0

};

export default (state: State = initialState, action: any) => {
  switch (action.type) {

    case t.ADD_PERCENT: {
      return { ...state, isLoad: true, error: null, percent: action.payload };
    }

    case t.ADD_DAY: {
      return { ...state, isLoad: true, error: null, day: action.payload };
    }
    default:
      return state;
  }
};