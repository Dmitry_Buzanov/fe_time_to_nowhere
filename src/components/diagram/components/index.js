// @flow
//<DiagramDay data={false} days={1}/>
//<DiagramPercent data={true} percents={22}/>
import * as React from "react";
import { Diagramm } from "./style/index.js";

export default (props): React.Node | null => {
  const { percent, day } = props;
  let bck = "";
  let datestart = new Date(2018, 8, 20);
  let dateend = new Date(2018, 8, 28);
  let result = (dateend - datestart) / 1000 / 24 / 60 / 60;

  let { diametr, strokeWidth } = props;
  let width = diametr;
  let height = width;
  let length = (diametr - strokeWidth) * Math.PI;
  let r = width / 2 - strokeWidth / 2;

  let valElPercent = (percent * length) / 100;
  let valElDays = (day * length) / result;

  if (percent >= 50 && percent < 75) {
    bck = "#D2C551";
  } else if (percent >= 75 && percent < 100) {
    bck = "#51D2B3";
  } else if (percent === 100) {
    bck = "#51BBD2";
  } else {
    bck = "#D25151";
  }

  if (props.small) {
    return (
      <Diagramm className={props.className}>
        <svg style={{ width, height }}>
          <circle
            className="circle_1"
            cx="50%"
            cy="50%"
            r={`${r}px`}
            style={{
              stroke: "#E5E5E5",
              strokeWidth: `${strokeWidth}`
            }}
          />
          <circle
            className="circle_2"
            cx="50%"
            cy="50%"
            r={`${r}px`}
            style={{
              strokeDasharray: `${valElPercent}, ${length}`,
              stroke: bck,
              strokeWidth: `${strokeWidth}`
            }}
          />
          <text
            x="50%"
            y="55%"
            textAnchor="middle"
            fill="black"
            className="textProcent1"
          >
            {percent}%
          </text>
        </svg>
      </Diagramm>
    );
  }
  if (props.data) {
    return (
      <Diagramm className="root2">
        <svg style={{ width, height }}>
          <circle
            className="circle_1"
            cx="50%"
            cy="50%"
            r={`${r}px`}
            style={{
              stroke: "#E5E5E5",
              strokeWidth: `${strokeWidth}`
            }}
          />
          <circle
            className="circle_2"
            cx="50%"
            cy="50%"
            r={`${r}px`}
            style={{
              strokeDasharray: `${valElDays}, ${length}`,
              stroke: bck,
              strokeWidth: `${strokeWidth}`
            }}
          />

          <text
            x="50%"
            y="53.5%"
            textAnchor="middle"
            fill="black"
            className="textProcent1"
          >
            {day}
          </text>
          <text x="35%" y="62.2%" className="textProcent2">
            Days left
          </text>
        </svg>
      </Diagramm>
    );
  }
  if (!props.data) {
    return (
      <Diagramm className="root2">
        <svg style={{ width, height }}>
          <circle
            className="circle_1"
            cx="50%"
            cy="50%"
            r={`${r}px`}
            style={{
              stroke: "#E5E5E5",
              strokeWidth: `${strokeWidth}`
            }}
          />
          <circle
            className="circle_2"
            cx="50%"
            cy="50%"
            r={`${r}px`}
            style={{
              strokeDasharray: `${valElPercent}, ${length}`,
              stroke: bck,
              strokeWidth: `${strokeWidth}`
            }}
          />

          <text
            x="50%"
            y="53.5%"
            textAnchor="middle"
            fill="black"
            className="textProcent1"
          >
            {percent}%
          </text>
          <text x="35%" y="62.2%" className="textProcent2">
            Finished
          </text>
        </svg>
      </Diagramm>
    );
  }
};
