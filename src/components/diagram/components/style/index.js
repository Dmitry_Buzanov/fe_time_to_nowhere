import styled from "styled-components";

export const Diagramm = styled.div`
  .percents {
    z-index: 2;
  }
  .cub {
    width: 92px;
    height: 92px;
    border-radius: 50%;
    border: 5px solid black;
  }
  .circle_1 {
    fill: #fff;
    stroke: red;
    stroke-dasharray: 773px 773px;
  }
  .circle_2 {
    fill: none;
    stroke: red;
    transform: rotate(-90deg);
    transform-origin: center;
    transition: 0.5s;
  }
  .perc {
    background-color: black;
    z-index: 9;
  }
  text {
    font-family: "Muli";
    font-weight: bold;
  }
  .textProcent1 {
    font-size: 48px;
  }
  .textProcent2 {
    font-size: 18px;
  }
  .trackDate {
    display: flex;
    flex-direction: row;
  }
`;
