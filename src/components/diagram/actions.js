// @flow

import * as t from './actionTypes';

export const addPercent = (data: any = {}) => ({
    type: t.ADD_PERCENT,
    payload: data
});
export const addDay = (data: any = {}) => ({
    type: t.ADD_DAY,
    payload: data
});


