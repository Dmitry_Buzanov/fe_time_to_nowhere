// @flow
import { NAME } from './constants';

export const ADD_PERCENT = `${NAME}/ADD_PERCENT`;
export const ADD_DAY = `${NAME}/ADD_DAY`;

