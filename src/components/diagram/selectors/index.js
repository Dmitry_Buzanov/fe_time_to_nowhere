// @flow

import { createSelector } from 'reselect';
import { NAME } from './../constants';

// export const getPageData = (state: any) => state[NAME].pageData;
// export const getLoader = (state: any) => state[NAME].isLoad;
export const getPercent = (state: any) => state[NAME].percent;
export const getDay = (state: any) => state[NAME].day;
// export const getError = (state: any) => state[NAME].error;


export const selectPercent = createSelector(getPercent, (percent: any) => {
  return percent;
});
export const selectDay = createSelector(getDay, (day: any) => {
  return day;
});


