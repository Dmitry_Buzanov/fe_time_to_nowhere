// @flow

import { connect } from 'react-redux';
import {  NAME } from './../constants';
import * as actionCreators from './../actions';
import { bindActionCreators } from 'redux';
import { 
  compose, 
  setDisplayName,
  defaultProps,
} from 'recompose'
import addReducer from "./../../../HOComponents/addReducer";
import Component from "./../components";
import reducers from "./../reducers";
import { 
  selectPercent, 
  selectDay,
} from './../selectors';

const mapStateToProps = (state: any) => ({
  percent: selectPercent(state),
  day: selectDay(state)
});

const mapDispatchToProps = (dispatch: any) => ({
  actions: bindActionCreators(actionCreators, dispatch)
});

export default compose(
  setDisplayName(`components/${NAME}`),
  connect(mapStateToProps, mapDispatchToProps),

  addReducer(NAME, reducers),
  defaultProps({
    location:{
      state: {}
    } 
  }),
)(Component);

