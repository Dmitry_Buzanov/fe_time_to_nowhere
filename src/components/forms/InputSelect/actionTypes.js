// @flow
import { NAME } from './constants';

export const SEARCH = `${NAME}/SEARCH`;
export const SEARCH_SUCCEEDED = `${NAME}/SEARCH_SUCCEEDED`;
export const SEARCH_FAILED = `${NAME}/SEARCH_FAILED`;
export const SELECT_CHANGE = `${NAME}/SELECT_CHANGE`;
export const UPDATE_FORM_STATE = `final-form-redux-example/finalForm/UPDATE_FORM_STATE`;
export const SETREF = `${NAME}/SETREF`;

export const REFRESH = `${NAME}/REFRESH`;
