// @flow
import * as t from './actionTypes';
import type {State} from './types';

export const initialState: State = {
    error: null,
    pageData: null,
    isSearch: false,
    refElement: null,
    value: null
};

export default (state: State = initialState, action: any) => {
    switch (action.type) {
        case t.UPDATE_FORM_STATE:
            return {
                ...state,
                [action.form]: action.payload
            };

        case t.SELECT_CHANGE:
            return {
                ...state,
                [action.form]: action.payload
            };

        case t.REFRESH: {
            return initialState;
        }

        default:
            return state;
    }
};