// @flow

import { NAME } from './../constants';
import watcherSearchData from './searchData';

export default {
    [`${NAME}-watcherSendForm`]: watcherSearchData,

};
  