import styled from "styled-components";

const background_color = '#ffffff';
const text_color = '#ffffff';

export const FormSearch = styled.form`
&.search_form input{
    background:transparent;
    padding:3px 0;
    border-width: 2px;
    border-color: ${background_color};
    color: ${text_color};
    border-top: none;
    border-left: none;
    border-right: none;
    width: 155px;
    height:21px;
    border-radius: 0;
}
&.search_form input::placeholder{
    color: ${text_color};
}
&.search_form input[type="search"]::-webkit-search-cancel-button {
    cursor: pointer; 
}
`;