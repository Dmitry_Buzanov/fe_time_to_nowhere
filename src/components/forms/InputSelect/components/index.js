import React from 'react';
import Select from 'react-select';

const InputSelect = (props): React.Node | null => {
        const { width, placeholder, options } = props;
        const { handleChange } = props.actions;

    const customStyles = {
        placeholder: (base) => ({
            ...base,
            fontFamily: 'Muli',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '14px',
            lineHeight: 'normal',
            color: '#393939',
            opacity: '.3'
    }),
        singleValue: (base) => ({
            ...base,
            fontFamily: 'Muli',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '14px',
            lineHeight: 'normal',
            color: '#393939',
            opacity: '.3'
    }),
        control: (base, state) => ({
            ...base,
            boxShadow: "none",
            borderRadius: "15px",
            outline: "none",
            width: width ? width : 'auto',

        })
    };
        return (

            <Select
                components={
                    {
                        /*DropdownIndicator: () => null,*/
                        IndicatorSeparator: () => null
                    }
                }
                styles={customStyles}
                placeholder={placeholder}
                onChange={{handleChange}}
                options={options}
            />
        );

};

export default InputSelect;