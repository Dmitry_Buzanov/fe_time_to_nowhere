// @flow

import * as t from './actionTypes';


export const updateFormState = (form, state) => ({
    type: t.UPDATE_FORM_STATE,
    form,
    payload: state
})

export const handleChange = (data) => ({
    type: t.SELECT_CHANGE,
    payload: data
})

export const refresh = () => ({
    type: t.REFRESH
});