// @flow
import { NAME } from './constants';

export const SEARCH = `${NAME}/SEARCH`;
export const SEARCH_FAILED = `${NAME}/SEARCH_FAILED`;
export const UPDATE_FORM_STATE = `final-form-redux-example/finalForm/UPDATE_FORM_STATE`;
export const SETREF = `${NAME}/SETREF`;
export const UPDATE_USER = `${NAME}/UPDATE_USER`;
export const OPEN_POPUP = `${NAME}/OPEN_POPUP`;
export const CLOSE_POPUP = `${NAME}/CLOSE_POPUP`;

export const REFRESH = `${NAME}/REFRESH`;

