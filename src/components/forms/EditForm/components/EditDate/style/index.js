import styled from "styled-components";

export const Header = styled.div`
width: 100%;
display: flex;
align-items: center;
justify-content: space-between;
max-width: 300px;
`;

export const InputWrapper = styled.div`
width: ${props => props.width || "100%"};

`;