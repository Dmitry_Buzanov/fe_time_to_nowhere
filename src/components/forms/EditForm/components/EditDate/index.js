// @flow
import * as React from "react";
import {Header, InputWrapper} from "./style/index"
import Input from "../../../Input";

const EditDate = (props): React.Node | null => {

    const createDate = () => {
            let i = 1;
            let date = [];
            while (i <= 31) {
                date.push({
                    value: i,
                    label: i
                });
                i+=1;
            }
            return date
        };
    const createYear = () => {
            let i = 1900;
            let date = [];
            while (i <= 2019) {
                date.push({
                    value: i,
                    label: i
                });
                i+=1;
            }
            return date
        };
    const createMonth = () => {
            let i = 1;
            let date = [];
            while (i <= 12) {
                if (i<10){
                    date.push({
                        value: '0' + i,
                        label: '0' + i
                    });
                }
                else {
                    date.push({
                        value:  i,
                        label:  i
                    });
                }

                i+=1;
            }
            return date
        };

    const {date, label} = props;
    const {day, month, year} = date;
    return (
        <Header>
            <InputWrapper width="66px">
                <Input
                    name="day"
                    type="select"
                    options={createDate()}
                    placeholder={day}
                    label={label}
                />
            </InputWrapper>
            <InputWrapper width="98px">
                <Input
                    name="month"
                    type="select"
                    options={createMonth()}
                    placeholder={month}
                />
            </InputWrapper>
            <InputWrapper width="92px">
                <Input
                    name="year"
                    type="select"
                    options={createYear()}
                    placeholder={year}
                />
            </InputWrapper>
        </Header>
    );
};
export default EditDate