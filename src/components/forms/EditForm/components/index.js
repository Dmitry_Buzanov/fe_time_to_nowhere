// @flow
import * as React from "react";
import {Form, FormSpy} from 'react-final-form'
import {EditWrapper, ButtonWrapper} from "./style/index";
import Input from "../../Input";
import EditProfileHeader from "./EditProfileHeader/index";
import EditDate from "./EditDate/index";
import Popup from "./PopupBecomeMentor/index";
import DefaultBtn from '../../../buttons/defaultBtn/index';



const EditForm = (props): React.Node | null => {
    const { name, surename, gender, image, mail, position, form, date } = props;
    const {sendForm, updateFormState, openPopup, closePopup} = props.actions;
    const options = [
        { value: 'male', label: 'male' },
        { value: 'female', label: 'female' }
    ];
    return (
            <EditWrapper>
                {props.popupIsOpen && <Popup closePopup={closePopup}/> }
                <div className="root">
                    <EditProfileHeader image={image} btnClick={openPopup} />
                    <Form
                        onSubmit={sendForm}
                        render={({handleSubmit}) => (
                            <form onSubmit={handleSubmit}>
                                <FormSpy onChange={(state) => updateFormState(form, state)}/>
                                <Input
                                    name="name"
                                    type="text"
                                    placeholder={name}
                                    label="name"
                                />
                                <Input
                                    name="surename"
                                    type="text"
                                    placeholder={surename}
                                    label="surename"
                                />
                                <Input
                                    name="gender"
                                    type="select"
                                    options={options}
                                    placeholder={gender}
                                    label="gender"
                                />
                                <EditDate date={date}/>
                                <Input
                                    name="position"
                                    type="text"
                                    placeholder={position}
                                    label="position held"
                                />
                                <Input
                                    name="e-mail"
                                    type="text"
                                    placeholder={mail}
                                    label="e-mail"
                                />
                                <ButtonWrapper>
                                    <DefaultBtn type="submit">save</DefaultBtn>
                                </ButtonWrapper>
                                {/*{errorData && <div className="text-danger">{errorData.message}</div>}*/}
                            </form>
                        )}
                    />
                </div>
            </EditWrapper>
    );
};
export default EditForm