import styled from "styled-components";

export const EditWrapper = styled.div`
margin: 0 auto;
max-width: 300px;

form{
width:100%;
}
`;

export const ButtonWrapper = styled.div`
margin-top: 14px;
button{
    margin:0 auto;
}
`;
