// @flow
import * as React from "react";
import {InputWrapper} from "./style/index"
import Popup from '../../../../popup/index';
import {Form} from 'react-final-form'
import Input from "../../../Input/index";
import {ButtonWrapper} from "../style";
import DefaultBtn from '../../../../buttons/defaultBtn/index';

const EditProfileHeader = (props): React.Node | null => {
    const popupContent =
        <Form
            onSubmit={{}}
            render={() => (
                <React.Fragment>
                <InputWrapper>
                    <Input
                        name="technology"
                        type="select"
                        placeholder="PHP"
                        label="technology"
                    />
                </InputWrapper>
                <ButtonWrapper>
                        <DefaultBtn type="submit">send the request</DefaultBtn>
                    </ButtonWrapper>
                </React.Fragment>
            )}/>
    ;
    return (
        <Popup message={popupContent} title='Became a mentor' onClick={props.closePopup}/>
    );
};
export default EditProfileHeader