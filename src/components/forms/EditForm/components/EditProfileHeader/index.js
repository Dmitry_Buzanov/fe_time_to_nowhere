// @flow
import * as React from "react";
import {Header, AvaterContainer} from "./style/index"
import DefaultButton from '../../../../buttons/defaultBtn/index';
import Icon from "../../../../../assets/Icon";
import * as i from "../../../../../assets/Icon/constants";

const EditProfileHeader = (props): React.Node | null => {
    const {image, name, btnClick} = props;
    return (
        <Header>
            <AvaterContainer>
                <img className="content-image" src={image} alt={name}/>
                <Icon type={i.DOWNLOAD} />
            </AvaterContainer>
            <DefaultButton onClick={btnClick}>became a mentor</DefaultButton>
        </Header>
    );
};
export default EditProfileHeader