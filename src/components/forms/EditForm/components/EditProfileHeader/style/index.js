import styled from "styled-components";

const background_color = '#51D2B3';

export const Header = styled.div`
width: 100%;
display: flex;
align-items: center;
justify-content: space-between;
max-width: 300px;
padding-bottom: 15px;
`;

export const AvaterContainer = styled.div`
position: relative;
svg{
fill:${background_color}
position: absolute;
bottom:0;
right:0;
}

img{
max-width: 90px;
border-radius:50%
}
`;