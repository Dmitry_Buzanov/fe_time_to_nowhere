import styled from "styled-components";

const input_height = '13px';
const input_width = '13px';
const border_color = '#393939';
const calc_check = '60px';

export const Input = styled.input`
    display: none;

& + label:before {
    content: "\\2713";
    color: transparent;
    display: inline-block;
    border: 2px solid ${border_color};
    border-radius: 2px;
    font-size: 17px;    
    line-height: 13px;
    margin: 20px 0 0 25px;
    height: ${input_height};
    position: relative;
    cursor: pointer;
    width: ${input_width};
    text-align: center;
    vertical-align: middle;
    transition: color ease .3s;
    z-index: 0;
    
}
&:checked + label:before {
    color: #000;
}
`;

export const InputWrapper = styled.div`
    position: absolute;
    left: 20px;
    display: flex;
    align-items: flex-end;
    height:100%;
    top: calc(50% - ${calc_check});
    
`;
