import React from 'react';
import {Input, InputWrapper} from "./style/index";

const Checkbox = ({onClick, id, isSelected}) => (
    <InputWrapper>
        <Input defaultChecked={!!isSelected} id={id} type="checkbox" className="checkbox"/>
      <label onClick={() => onClick(id)} htmlFor={id}/>
    </InputWrapper>
)



export default Checkbox