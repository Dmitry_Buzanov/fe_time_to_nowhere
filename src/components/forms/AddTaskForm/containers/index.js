// @flow

import { connect } from 'react-redux';
import { FORM_NAME, NAME } from './../constants';
import * as actionCreators from './../actions';
import { bindActionCreators } from 'redux';
import { 
  compose, 
  setDisplayName,
  defaultProps,
  withHandlers,
  lifecycle,
  withProps
} from 'recompose';
import addSaga from "./../../../../HOComponents/addSaga";
import addReducer from "./../../../../HOComponents/addReducer";
import Component from "./../components";
import reducers from "./../reducers";
import sagas from "./../sagas";
import {
    countTasks
} from './../selectors';

const mapStateToProps = (state: any) => ({
    countTasks: countTasks(state)
});

const mapDispatchToProps = (dispatch: any) => ({
  actions: bindActionCreators(actionCreators, dispatch)
});

export default compose(
  setDisplayName(`components/${NAME}`),
  connect(mapStateToProps, mapDispatchToProps),
  addSaga({sagas}), 
  addReducer([NAME], reducers),
  withHandlers({
    updateFormState:  props => (values: any) => () => {
      return props.actions.updateFormState(values);
    },
    sendForm: props => (values: any) => () => {
      return props.actions.sendForm(values);
    },
      addTask: props => () => () => {
      return props.actions.addTask();
    },
    closePopup: props => () => () => {
      return props.actions.closePopup();
    },
  }),
  withProps({
    form:FORM_NAME
  }),
  lifecycle({
    componentDidUpdate(){
      if(this.props.isFocused)
      this.props.refElement.focus()
    },
    componentWillUnmount(){
      this.props.actions.refresh();
    }
  }),
  defaultProps({
    location:{
      state: {}
    } 
  }),
)(Component);

