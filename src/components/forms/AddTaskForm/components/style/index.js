import styled from "styled-components";


export const AddTaskWrapper = styled.div`
margin: 0 auto;

form{
width:100%;
}
`;

export const ContentWrapper = styled.div`
display:flex;
justify-content:space-between;
`;

export const ButtonWrapper = styled.div`
margin-top: 14px;
button{
    margin:0 auto;
}
`;
