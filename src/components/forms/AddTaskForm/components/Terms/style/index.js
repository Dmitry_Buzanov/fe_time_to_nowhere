import styled from "styled-components";

export const Wrapper = styled.div`
width: 100%;
position: relative;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
max-width: 300px;
`;

export const BtnWrapper = styled.div`
position: absolute;
bottom: 0;
right: 0;
`;