import * as React from "react";
import {Wrapper, BtnWrapper} from "./style/index";
import EditDate from "../../../EditForm/components/EditDate";
import DefaultBtn from "../../../../buttons/defaultBtn";

const Terms = (): React.Node | null => {
    const date = {
        day: '21',
        month: '02',
        year: '2019'
    };
    return (
        <Wrapper>
            <EditDate label="start date" date={date}/>
            <EditDate label="deadline" date={date}/>
            <BtnWrapper>
                <DefaultBtn>task base</DefaultBtn>
            </BtnWrapper>
        </Wrapper>
    );
};
export default Terms