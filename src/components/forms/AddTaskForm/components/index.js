// @flow
import * as React from "react";
import {Form, FormSpy} from 'react-final-form'
import {AddTaskWrapper, ContentWrapper} from "./style/index";
import CommonInfo from './CommonInfo';
import Tasks from './Tasks';
import Terms from './Terms';


const AddTaskForm = (props): React.Node | null => {
    const {form, countTasks} = props;
    const {sendForm, updateFormState, addTask} = props.actions;

    return (
        <AddTaskWrapper>
            <Form
                onSubmit={sendForm}
                render={({handleSubmit}) => (
                    <form onSubmit={handleSubmit}>
                        <FormSpy onChange={(state) => updateFormState(form, state)}/>
                        <ContentWrapper>
                            <CommonInfo/>
                            <Tasks addTask={addTask} countTasks={countTasks}/>
                            <Terms/>
                        </ContentWrapper>
                    </form>
                )}
            />
        </AddTaskWrapper>
    );
};
export default AddTaskForm