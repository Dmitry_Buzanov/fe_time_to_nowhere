// @flow
import * as React from "react";
import {Wrapper, InputWrapper} from "./style/index"
import Input from "../../../Input";
import DefaultBtn from '../../../../buttons/defaultBtn/index';

const CommonInfo = (): React.Node | null => {

    return (
        <Wrapper>
            <InputWrapper>
                <Input
                    name="sphere"
                    type="text"
                    placeholder='UX/UI Design'
                    label="sphere"
                />
            </InputWrapper>

            <InputWrapper>
                <Input
                    rows={10}
                    height='200px'
                    name="sphereTextarea"
                    type="textarea"
                    placeholder='Some hardcore tasks'
                    label="description"
                />
            </InputWrapper>
                <DefaultBtn type="submit">save</DefaultBtn>
        </Wrapper>
    );
};
export default CommonInfo