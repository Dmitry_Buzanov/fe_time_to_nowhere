import styled from "styled-components";

export const Wrapper = styled.div`
width: 100%;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: space-between;
max-width: 300px;
`;

export const InputWrapper = styled.div`
width: ${props => props.width || "100%"};

`;