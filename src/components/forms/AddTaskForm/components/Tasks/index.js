// @flow
import * as React from "react";
import {Wrapper, InputWrapper, BtnWrapper} from "./style/index"
import Input from "../../../Input";
import Icon from "../../../../../assets/Icon";
import * as i from "../../../../../assets/Icon/constants";
import PlusBtn from '../../../../buttons/plusBtn';

const Tasks = (props): React.Node | null => {
    const {addTask} = props;
    return (
        <Wrapper>
            <InputWrapper>
                <Input
                    name="task1"
                    type="text"
                    placeholder='UX/UI Design'
                    label="tasks"
                    iconAppend={<Icon type={i.BASKET} />}
                />
            </InputWrapper>
            <InputWrapper>
                <Input
                    name="task2"
                    type="text"
                    placeholder='UX/UI Design'
                    iconAppend={<Icon type={i.BASKET} />}
                />
            </InputWrapper>
            <InputWrapper>
                <Input
                    name="task3"
                    type="text"
                    placeholder='UX/UI Design'
                    iconAppend={<Icon type={i.BASKET} />}
                />
            </InputWrapper>
            <BtnWrapper>
                <PlusBtn onClick={addTask}/>
            </BtnWrapper>

        </Wrapper>
    );
};
export default Tasks