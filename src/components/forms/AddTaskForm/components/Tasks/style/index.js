import styled from "styled-components";

export const Wrapper = styled.div`
width: 100%;
display: flex;
flex-direction: column;
align-items: flex-end;
justify-content: flex-start;
max-width: 300px;
padding: 6px 0 43px 0;
position relative;
svg{
fill: #717171;
}
`;

export const InputWrapper = styled.div`
width: ${props => props.width || "100%"};
> div{
    padding: 10px 0 !important;
}

`;
export const BtnWrapper = styled.div`
    position: absolute;
    bottom:0;
    right:0;
`;