// @flow
import * as t from './actionTypes';
import type { State } from './types';

export const initialState: State = {
  error: null,
  pageData: null,
  isLoad: false,
  countTasks: [{},{},{}]
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case t.UPDATE_FORM_STATE:
      return {
        ...state,
        [action.form]: action.payload
      };
    case t.ADD_TASK: {
      return { ...state, countTasks: state.countTasks.push({}) };
    }
    case t.CLOSE_POPUP: {
      return { ...state, popupIsOpen: false};
    }
    case t.UPDATE_USER: {
      return { ...state, isLoad: true, error: null };
    }
    case t.REFRESH: {
      return initialState;
    }

    default:
      return state;
  }
};