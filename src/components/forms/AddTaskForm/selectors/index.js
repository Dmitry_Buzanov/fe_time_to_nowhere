// @flow

import { createSelector } from 'reselect';
import { NAME } from './../constants';

export const getPageData = (state: any) => state[NAME].countTasks;
export const getSearcher = (state: any) => state[NAME].isLoad;
export const getError = (state: any) => state[NAME].error;
export const getRefElement = (state: any) => state[NAME].refElement;


export const countTasks = createSelector(getPageData, (pageData: any) => {
  return pageData;
});

