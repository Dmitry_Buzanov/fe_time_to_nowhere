// @flow

import * as React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import { Field, FieldProps } from 'react-final-form';
import { InputWrapper} from "./style/index.js";
import Select from "../InputSelect/index";

type RenderProps = {
  help: ?string | React.Element<'div'> | React.Element<'span'> | null,
  label: string,
  icon: ?any,
  forID: ?string,
  iconAppend: ?any,
  iconPrepend: ?any,
  iconButtonAppend: ?any,
  inputRef: ?Function,
} & FieldProps;

const renderInput = ({
  input,
  meta,
  meta: { asyncValidating, error, warning },
  help,
  label,
  icon,
  forID,
  inputRef,
  align,
  iconAppend,
  iconPrepend,
  iconButtonAppend,
  required,
  touched, options, width,
  ...rest,
  type
}: RenderProps) => {
  touched = touched || meta.touched;
  const formClass: string = classNames({
    'form-group': true,
    [`text-${(align && align) || 'left'}`]: true,
    'async-validating': !!asyncValidating,
    required: required
  });

  return (
    <InputWrapper className={formClass}>
      <div className="input-group">
        {(icon || iconPrepend) && (
          <div className="input-group-prepend">
            <span className="input-group-text">{icon || iconPrepend}</span>
          </div>
        )}
        {type === "select" ? <Select
            ref={inputRef}
            id={forID}
            width={width}
            value={{}}
            onChange={{}}
            options={options}
            {...input}
            {...rest}
        /> : type === 'textarea' ? <textarea
            ref={inputRef}
            className={touched && (error || warning) ? ("form_textarea input_error"): touched && type==="password" ? ("form_textarea input_success") : ("form_textarea")}
            id={forID}
            {...input}
            {...rest}
        /> : <input
            ref={inputRef}
            className={touched && (error || warning) ? ("form_input input_error"): touched && type==="password" ? ("form_input input_success") : ("form_input")}
            id={forID}
            {...input}
            {...rest}
        />
        }

        {label && (
        <label className="form_label" htmlFor={forID}>
          {label}
        </label>
        )}
        {iconAppend && (
          <div className="input-group-append">
            <span className="input-group-text">{iconAppend}</span>
          </div>
        )}
        {iconButtonAppend && (
          <div className="input-group-append">{iconButtonAppend}</div>
        )}
        {touched && error && <small className="form_error">{error}</small>}
      </div>
      {help && <small className="form_help">{help}</small>}
    </InputWrapper>
  );
};

type Props = {
  name: string,
  inputRef: ?Function,
  type: ?string,
  help: ?string | React.Element<'div'> | React.Element<'span'> | null,
  label: ?string,
  forID: ?string,
  icon: ?any,
  iconAppend: ?any,
  iconPrepend: ?any,
  iconButtonAppend: ?any,
  align: ?string
};

class Input extends React.Component<Props, any> {
  static defaultProps: Props = {
    name: 'name',
    type: 'text',
    inputRef: null,
    help: null,
    label: null,
    forID: null,
    icon: null,
    iconAppend: null,
    iconPrepend: null
  };
  format: Function;
  parse: Function;
  constructor() {
    super(...arguments);
    this.format = this.format.bind(this);
    this.parse = this.parse.bind(this);
  }
  format(value: string | number) {
    if (this.props.type === 'date') {
      return value ? moment(value).format('YYYY-MM-DD') : '';
    }
    if ( typeof value === 'number') {
      return value.toString();
    }
    return value || '';
  }
  parse(value: string) {
    if (this.props.type === 'date') {
      return value ? moment(value).format('YYYY-MM-DD HH:mm:ss.000000') : '';
    }
    return value || '';
  }
  render(): React.Node | null {
    const { name, ...rest } = this.props;

    return (
      <Field
        name={name}
        format={this.format}
        parse={this.parse}
        component={renderInput}
        {...rest}
      />
    );
  }
}

export default Input;
