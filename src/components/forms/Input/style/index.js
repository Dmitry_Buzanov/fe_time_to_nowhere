import styled from "styled-components";

const main_color = "#393939";
const error_color = "#D25151";
const success_color = "#51D2B3";

export const InputWrapper = styled.div`
  &.form-group {
    width: 100%;
    padding: 16px 0;
    position: relative;
  }
  a:focus,
  button:focus,
  input:focus,
  textarea:focus {
    outline: none;
  }
  .input-group-text {
    position: absolute;
    right: 10px;
    top: 10px;
    cursor: pointer;
  }
  .input-group {
    position: relative;
  }
  .form_textarea {
    font-family: "Muli", sans-serif;
    width: 100%;
    resize: none;
    border: 1px solid ${main_color};
    box-sizing: border-box;
    border-radius: 15px;
    padding: 11px 18px;
    font-size: 14px;
    height: 140px;
    opacity: 0.3;
  }
  .form_input {
    font-family: "Muli", sans-serif;
    width: 100%;
    height: 40px;
    border: 1px solid ${main_color};
    box-sizing: border-box;
    border-radius: 15px;
    padding: 11px 18px;
    font-size: 14px;
    opacity: 0.3;
  }
  .input_error::placeholder {
    color: ${error_color} !important;
  }
  .input_error {
    border-color: ${error_color} !important;
    border: 1px solid;
    color: ${error_color} !important;
    opacity: 1;
  }
  .input_error + &.form_label {
    color: ${error_color} !important;
    opacity: 1;
  }
  .input_success::placeholder {
    color: ${success_color} !important;
  }
  .input_success {
    border-color: ${success_color} !important;
    border: 1px solid;
    color: ${success_color} !important;
    opacity: 1;
  }
  .input_success + &.form_label {
    color: ${success_color} !important;
    opacity: 1;
  }
  .form_input:focus + &.form_label,
  .form_input:focus {
    opacity: 1;
  }
  .form_textarea:focus + &.form_label,
  .form_textarea:focus {
    opacity: 1;
  }
  .form_label {
    font-family: "Muli", sans-serif;
    position: absolute;
    top: -17px;
    display: block;
    font-style: normal;
    font-weight: normal;
    line-height: normal;
    font-size: 10px;
    color: ${main_color};
    opacity: 0.3;
    padding-bottom: 4px;
  }
  .form_error {
    width: 100%;
    bottom: -15px;
    text-align: center;
    position: absolute;
    font-weight: 600;
    font-size: 12px;
    left: 0;
    color: ${error_color};
  }
`;
