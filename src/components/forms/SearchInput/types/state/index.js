// @flow
export default type State = {
    isSearch: boolean,
    error: any,
    pageData: any,
    refElement: any
};