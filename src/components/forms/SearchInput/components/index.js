// @flow
import * as React from "react";
import { Form, FormSpy } from 'react-final-form'
import { FormSearch } from "./style/index";
import Input from "../../Input";

const SearchInput = (props): React.Node | null => {
  const { errorData,form,onBlur} = props;

  const {searchData,updateFormState,setRef} = props.actions;
  
  return (
            <Form
                 onSubmit={searchData}
                 render={({ handleSubmit }) => (
                  <FormSearch
                    onSubmit={handleSubmit}
                    className="search_form">
                    <FormSpy onChange={(state) => updateFormState(form, state)} />
                       <Input
                        inputRef={(el)=>{if(el!==null)setRef(el)}}
                        name="search"
                        type="search"
                        placeholder="Search"
                        onBlur={onBlur}
                       />
                      {errorData && <div className="text-danger">{errorData.message}</div>}
                </FormSearch>
                )}
            />
  );
};


export default SearchInput