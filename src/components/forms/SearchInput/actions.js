// @flow

import * as t from './actionTypes';

export const searchData = (data: any = {}) => ({
    type: t.SEARCH,
    payload: data
});

export const setRef = (refElement) => ({
    type: t.SETREF,
    refElement: refElement
})

export const updateFormState = (form, state) => ({
    type: t.UPDATE_FORM_STATE,
    form,
    payload: state
})

export const refresh = () => ({
    type: t.REFRESH
});