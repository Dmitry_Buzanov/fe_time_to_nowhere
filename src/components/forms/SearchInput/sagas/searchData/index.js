import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetch from './../../../../../api';
import * as contextT from './../../../../../features/context/actionTypes'

export function* searchData(action: any): any {
  try {
    const response = yield call(fetch.get, '/search');
    yield put({
      type: contextT.GET_USER_DATA,
      payload: {email:response.email}
    });
    yield put({
      type: contextT.ADD_CONTEXT,
      payload: response.data
    });
    yield put({
      type: t.SEARCH_SUCCEEDED,
      payload: { ...response, user_id: action.payload.username }
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.SEARCH_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}

export default function* watcherSearchData(): any {
  yield takeEvery(t.SEARCH, searchData);
}