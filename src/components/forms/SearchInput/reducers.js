// @flow
import * as t from './actionTypes';
import type { State } from './types';

export const initialState: State = {
  error: null,
  pageData: null,
  isSearch: false,
  refElement: null
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case t.SEARCH: {
      return { ...state, isSearch: true, error: null };
    }
    case t.UPDATE_FORM_STATE:
      return {
        ...state,
        [action.form]: action.payload
      }
    case t.SEARCH_FAILED: {
      return { ...state, isSearch: false, error: action.error };
    }
    case t.SEARCH_SUCCEEDED: {
      return { ...state, isSearch: false, pageData: action.payload };
    }
    case t.SETREF: {
      return {...state, refElement:action.refElement}
    }
    case t.REFRESH: {
      return initialState;
    }

    default:
      return state;
  }
};