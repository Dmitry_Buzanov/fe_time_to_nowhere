// @flow

import { createSelector } from 'reselect';
import { NAME } from './../constants';

export const getPageData = (state: any) => state[NAME].pageData;
export const getSearcher = (state: any) => state[NAME].isLoad;
export const getError = (state: any) => state[NAME].error;
export const getRefElement = (state: any) => state[NAME].refElement;

export const selectSearcher = createSelector(getSearcher, (isSearch: any) => {
  return isSearch;
});

export const selectPageData = createSelector(getPageData, (pageData: any) => {
  return pageData;
});

export const selectError = createSelector(getError, (error: any) => {
  return error;
});

export const selectRefElement = createSelector(getRefElement, (refElement: any) => {
  return refElement;
});