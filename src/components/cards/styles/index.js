import styled from "styled-components";

export const Card = styled.div`
  width: 260px;
  height: 200px;
  box-shadow: 0 10px 25px rgba(0, 0, 0, 0.1);
  border-radius: 20px;
  font-family: Muli, sans-serif;
  box-sizing: border-box;
  position: relative;
`;
