import React from "react";

import {
  PropgressCard,
  SphereName,
  Statisticks,
  StatistikcsItem,
  ProgressBar,
  RemainingTasks,
  TasksInfo,
  TaskInfoItem,
  DatesInfo,
  StartDate,
  DeadLine
} from "./styles/index";


export default (props) => {
  const { task } = props;
  return (
    <PropgressCard>
      <SphereName>{task.title}</SphereName>
      <Statisticks>
        {task.tasksCompleted &&
        <StatistikcsItem>
          <span>tasks completed: </span>
          {task.tasksCompleted}
        </StatistikcsItem>}
        {task.tasksToPromotion &&
        <StatistikcsItem>
          <span>tasks till the promotion: </span>
          {task.tasksToPromotion}
        </StatistikcsItem>}
      </Statisticks>
      <ProgressBar {...props} />
      <RemainingTasks>Remaining tasks:</RemainingTasks>
      <TasksInfo>
        {task.remainingTasks ? task.remainingTasks.map((task, index) => {
          const { text, dateInfo } = task;
          return (
            <TaskInfoItem key={index}>
              <div>{text}</div>
              <DatesInfo>
                <StartDate>
                  <span>Start Date: </span>
                  {dateInfo.startDate}
                </StartDate>
                <DeadLine>
                  <span>Start Date: </span>
                  {dateInfo.endDate}
                </DeadLine>
              </DatesInfo>
            </TaskInfoItem>);
        }) : <TaskInfoItem>No tasks</TaskInfoItem>}
      </TasksInfo>
    </PropgressCard>
  );
};
