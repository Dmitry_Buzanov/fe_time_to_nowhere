import styled from "styled-components";
import Diagram from '../../../../diagram/components/index';

export const PropgressCard = styled.section`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 477px;
  padding: 10px;
  font-size: 14px;
  border-right: 0.5px solid rgba(113, 113, 113, 0.5);
  box-sizing: border-box;
`;

export const SphereName = styled.h2`
  font-family: Muli, sans-serif;
  line-height: normal;
  font-size: 32px;
  margin-bottom: 10px;
  margin-left: 14px;
`;

export const Statisticks = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type:none;
  margin-bottom: 37px;
`;

export const StatistikcsItem = styled.li`
  position: relative;
  line-height: 26px;
  font-weight: bold;
  padding: 0 26px;
  & > span {
    font-weight: normal;
  }
   ::before{
   content: '';
   display: block;
   width: 5px;
   height: 5px;;
   border-radius: 2.5px;
   background-color: #393939;
   position: absolute;
   left: 14px;
   top: 13px
   overflow: hidden;
`;

export const ProgressBar = styled(Diagram)`
  position: absolute;
  top: 42px;
  right: 50px;
  & > svg {
    width: 92px;
    height: 92px;
    & .textProcent1 {
      font-size: 18px;
      }
  }
`;

export const RemainingTasks = styled.h3`
  margin-top: 0;
  margin-left: 14px;
  line-height: normal;
  font-size: 18px;
  color: #d25151;
`;

export const TasksInfo = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  width: 450px;
`;

export const TaskInfoItem = styled.li`
  position: relative;
  padding: 10px 26px;
  box-sizing: border-box;
  border-radius: 5px;
  font-family: Open Sans;
  line-height: 20px;
  border: 2px solid transparent;
  border-radius: 5px;
  & > div:first-child {
      display: inline-flex;
      width: 305px;
      margin-bottom: 5px;
  }
  :hover {
    border: 2px solid #51bbd2;
    box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.15);
    transition: all 0.25s;
  }
  
  ::before{
   content: '';
   display: block;
   width: 5px;
   height: 5px;
   border-radius: 2.5px;
   background-color: #393939;
   position: absolute;
   left: 14px;
   top: 16px
   overflow: hidden;
`;

export const DatesInfo = styled.div`
  display: flex;
  font-family: Muli, sans-serif;
  justify-content: space-between;
`;

export const StartDate = styled.div`
  & > span {
    font-weight: bold;
  }
`;

export const DeadLine = styled.div`
 & > span {
    font-weight: bold;
  }
`;
