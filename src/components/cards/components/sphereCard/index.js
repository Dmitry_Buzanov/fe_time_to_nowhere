import React from "react";
import Icon from "../../../../assets/Icon/index";
import { BASKET } from "../../../../assets/Icon/constants";
import StatusList from "../sphereCard/statusList/index";
import {
  Sphere,
  Header,
  Title,
  UserStatus,
  IconWrapper
} from "./styles/index";

export default (props) => {
  const { className, name, status, onClick } = props;
  return (
    <Sphere className={className}>
      <Header>
        <Title>{name}</Title>
        <UserStatus userStatus={status}>{status}</UserStatus>
      </Header>
      <StatusList {...props} />
      <IconWrapper onClick={onClick}>
        <Icon type={BASKET} alt={BASKET}/>
      </IconWrapper>
    </Sphere>
  );
};
