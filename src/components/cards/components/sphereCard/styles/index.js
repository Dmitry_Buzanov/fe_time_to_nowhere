import styled from "styled-components";
import { Card } from "../../../styles/index";
const baseFontWeight = "bold";

const userStatus = {
  mentor: "Mentor",
  student: "Student"
};

export const Sphere = styled(Card)`
  padding: 27px;
  font-size: 12px;
`;
export const Header = styled.div`
  align-items: baseline;
  justify-content: space-between;
  display: flex;
  margin-bottom: 18px;
`;
export const Title = styled.div`
  font-size: 24px;
  font-weight: ${baseFontWeight}
  margin-top: 5px;
  margin-bottom: 0;
`;
export const UserStatus = styled.div`
  font-weight: ${baseFontWeight}
  font-size: 14px;
  color: ${props =>
    props.userStatus === userStatus.mentor ? "#D25151" : "#51D2B3"};
  display: block;
`;
export const StatusList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;
export const ListItem = styled.li`
  line-height: 24px;
  &:last-child {
    margin-bottom: 0;
  }
`;
export const Key = styled.span`
  font-weight: ${baseFontWeight};
  margin-right: 5px;
`;
export const Value = styled.span``;

export const IconWrapper = styled.div`
  position: absolute;
  left: 208px;
  top: 146px;
  cursor: pointer;
  & > svg {
    fill: #393939;
  }
`;
