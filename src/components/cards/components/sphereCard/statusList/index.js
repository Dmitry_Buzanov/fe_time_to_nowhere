import React from "react";
import { statusKeys } from "./constants";
import {
  StatusList,
  ListItem,
  Key,
  Value
} from "../../sphereCard/styles/index";

export default ({ className, level, tasksDone, tasksCreated, students }) => {
  const statusValues = [level, tasksDone, tasksCreated, students];
  return (
    <StatusList className={className}>
      {statusValues.map((item, index) => {
        return (
          <ListItem key={index}>
            <Key>{statusKeys[index]}</Key>
            <Value>{item}</Value>
          </ListItem>
        );
      })}
    </StatusList>
  );
};
