import styled from "styled-components";
import { Card } from "../../../styles/index";

export const AddSphere = styled(Card)`
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  cursor: pointer;
  margin-right: 49px;
  & > svg {
    width: ${props => (props.own ? "79px" : "70px")};
    height: ${props => (props.own ? "79px" : "70px")};
    fill: ${props => (props.own ? "#51D2B3" : "#51BBD2")};
    margin-bottom: 23px;
    }
   &:last-child {
     margin-right: 0;
   }
`;

export const Text = styled.span`
   font-family: Muli, sans-serif;
   font-style: normal;
   font-weight: bold;
   line-height: normal;
   font-size: 14px;
   text-align: center;
   color: ${props => (props.own ? "#51D2B3" : "#51BBD2")}
   width: 132px;
   margin-bottom: 33px    
`;
