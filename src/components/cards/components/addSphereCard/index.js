import React from "react";
import Icon from "../../../../assets/Icon/index";
import { PLUS, PENCIL } from "../../../../assets/Icon/constants";
import { AddSphere, Text } from "./styles/index";

export default props => {
  const { children, onClick } = props;
  const type = props.own ? PENCIL : PLUS;

  return (
    <AddSphere onClick={onClick}>
      <Icon className="icon" type={type} alt={type} />
      <Text own={props.own}>{children}</Text>
    </AddSphere>
  );
};
