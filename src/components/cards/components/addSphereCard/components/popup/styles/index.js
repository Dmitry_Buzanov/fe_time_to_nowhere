import styled from "styled-components";


export const Background = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.9);
  z-index: 99;
`;
export const CardContainer = styled.div`
   display: flex;
   position: absolute;
   top: 50%;
   left: 50%
   transform: translate(-50%, -50%);
   z-index: 300;
`;