import React, { Fragment } from "react";
import AddSphere from "./../../index";
import { Background, CardContainer } from "./styles/index";
import {Link} from "react-router-dom";

export default (props) => {
  return (
    <Fragment>
      <Background onClick={props.onClick}/>
      <CardContainer>
        <AddSphere own onClick={props.onCardClick}>Create your own knowledge sphere</AddSphere>
          <Link to="/learning">
              <AddSphere onClick={props.onCardClick}>Choose knowledge sphere from the list</AddSphere>
          </Link>
      </CardContainer>
    </Fragment>
  );
}
