import React from 'react';
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";
import Button from "../defaultBtn";
import classnames from 'classnames';

export default ({className,...props}) =>{
    const classname = classnames({
        "download_btn": true
    }, className);
    
    return(
        <Button {...props} className={classname}>
            <Icon type={i.DOWNLOAD} />
        </Button>
    )
}