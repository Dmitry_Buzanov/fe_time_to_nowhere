import React from 'react';
import Button from "../defaultBtn";
import classnames from 'classnames';

export default ({className,...props}) =>{
    const classname = classnames({
        "plusTag_btn": true
    }, className);
    
    return(
        <Button className={classname} {...props} >
            <span></span>
        </Button>
    )
}