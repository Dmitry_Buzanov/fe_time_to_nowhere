import React from 'react';
import Button from "../defaultBtn";
import classnames from 'classnames';
import Icon from "../../../assets/Icon";
import * as i from "../../../assets/Icon/constants";

export default ({className,...props}) =>{
    const classname = classnames({
        "remove_btn": true
    }, className);
    return(
        <Button className={classname} {...props}>
            <Icon type={i.BASKET} />
        </Button>
    )
}