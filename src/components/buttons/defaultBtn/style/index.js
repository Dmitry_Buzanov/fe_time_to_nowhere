import styled from "styled-components";

export const DefaultButton = styled.button`
&.default_btn {
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 150px;
  min-height: 40px;
  background: #51D2B3;
  border-radius: 25px;
  font-family: 'Muli', sans-serif;
  font-size: 14px;
  color: #F4F4F4;
  text-align: center;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
  &:active{
    box-shadow: none;
}}
    &.closeButton {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 35px;
  height: 35px;
  position: relative;
  cursor: pointer;
}

&.closeButton span {
  position: absolute;
  display: block;
  height: 5px;
  width: 35px;
  background-color: #51D2B3;
}

&.closeButton span:nth-child(1) {
   transform: rotate(45deg);
}

&.closeButton span:nth-child(2) {
   transform: rotate(-45deg);
}

&.download_btn svg {
  fill: #51D2B3;
}

&.infinity_btn__left_btn, &.infinity_btn__right_btn {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  align-content: center;
  height: 42px;
  width: 42px;
  background-color: #51D2B3;
  border-radius: 50%;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
}
&.infinity_btn__left_btn:active, &.infinity_btn__right_btn:active{
    box-shadow: none;
}


&.plus_btn {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
          justify-content: center;
          align-items: center;
  width: 43px;
  height: 43px;
  background: #51D2B3;
  border-radius: 50%;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
}
&.plus_btn:active {
    box-shadow: none;
}

&.plus_btn span {
  display: block;
  width: 23px;
  height: 4px;
  position: relative;
}

&.plus_btn span::before, &.plus_btn span::after {
  top: 0;
  left: 0;
  display: block;
  position: absolute;
  content: "";
  width: 100%;
  height: 100%;
  background: #F4F4F4;
}

&.plus_btn span::after {
          transform: rotate(90deg);
}

&.plusTag_btn {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
          justify-content: center;
          align-items: center;
  padding: 0 8px;
  height: 22px;
  min-width: 37px;
  font-size: 24px;
  font-weight: bold;
  background: #51D2B3;
  border-radius: 25px;
  color: #F4F4F4;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
}
&.plusTag_btn:active {
    box-shadow: none;
}

&.plusTag_btn span {
  display: block;
  width: 12px;
  height: 2px;
  position: relative;
}

&.plusTag_btn span::before, &.plusTag_btn span::after {
  top: 0;
  left: 0;
  display: block;
  position: absolute;
  content: "";
  width: 100%;
  height: 100%;
  background: #F4F4F4;
}

&.plusTag_btn span::after {
  transform: rotate(90deg);
}

&.remove_btn svg {
  fill: #393939;
}

&.tag_btn {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 8px;
  height: 22px;
  background: #51D2B3;
  border-radius: 25px;
  font-family: 'Muli', sans-serif;
  font-size: 10px;
  color: #F4F4F4;
  text-align: center;
}

`;