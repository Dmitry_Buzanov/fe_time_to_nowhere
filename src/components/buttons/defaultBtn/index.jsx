import React from 'react';
import classnames from 'classnames';
import {
    compose,
    defaultProps
} from 'recompose';
import { DefaultButton } from "./style/index.js";





const Button = ({ children, className, ...props }) => {
    const classname = classnames({
        [className]: !!className,
        "default_btn": !className
    })
    return (

        <DefaultButton className={classname} {...props}>
            {children}
        </DefaultButton>
    );
};

export default compose(
    defaultProps({
        type: 'button'
    })
)(Button);