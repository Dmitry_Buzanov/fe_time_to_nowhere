import React from 'react';
import Button from "../defaultBtn";
import { InfinityButton } from "./style/index.js";
import Icon from "../../../assets/Icon";
import classnames from 'classnames';

export default ({leftBtnClass,rightBtnClass,leftBtnIco,leftBtnOnClick,rightBtnIco,rightBtnOnClick,leftBtnType,rightBtnType}) =>{
    const leftClassname = classnames({
        "infinity_btn__left_btn": true
    }, leftBtnClass);

    const rightClassname = classnames({
        "infinity_btn__right_btn": true
    }, rightBtnClass);

    return(
        <div className="infinity_btn_wrapper">
            <InfinityButton className="infinity_btn" >
                <Button className={leftClassname} onClick={leftBtnOnClick} type={leftBtnType}>
                    <Icon type={leftBtnIco}/>
                </Button>
                <span></span>
                <Button className={rightClassname} onClick={rightBtnOnClick} type={rightBtnType}>
                    <Icon type={rightBtnIco}/>
                </Button>
            </InfinityButton>
        </div>
    )
}