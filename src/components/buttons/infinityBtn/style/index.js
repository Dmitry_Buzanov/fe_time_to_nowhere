import styled from "styled-components";

const btn_background = '#51D2B3';
const background_color = '#ffffff';
const svg_color = '#ffffff';
const height = '42px';
const halfheight = '21px';
const width = '93px';
const hwinnerCircle = '30px';
const innerCirclesPadding = '19px';

export const InfinityButton = styled.div`


&.infinity_btn{
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
    height: ${height};
    width: ${width};
}
&.infinity_btn svg{
    fill: ${svg_color};
    z-index: 2;
    width: 20px;
    height: 20px;
}
&.infinity_btn span{
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    background-color: ${btn_background};
    width: ${hwinnerCircle};
    height: ${hwinnerCircle};
    border-radius: 50%;
}
&.infinity_btn span::before,&.infinity_btn span::after{
    content: '';
    height: ${halfheight};
    width: ${halfheight};
    background-color:${background_color};
    position: absolute;
    border-radius: 50%; 
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
}
&.infinity_btn span::before{
    margin-top: -${innerCirclesPadding};
}
&.infinity_btn span::after{
    margin-top: ${innerCirclesPadding};
}
&.infinity_btn__right_btn{
    padding-right: 2px;
}
`;