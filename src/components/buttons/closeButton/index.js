import React from 'react';
import Button from "../defaultBtn";
import classnames from 'classnames';


export default ({className,...props}): React.Node | null => {
    const classname = classnames({
        "closeButton": true
    }, className);
    
    return(
        <Button className={classname} {...props}>
            <span></span>
            <span></span>
        </Button>
    )
}