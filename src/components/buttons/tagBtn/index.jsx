import React from 'react';
import Button from "../defaultBtn";
import classnames from 'classnames';

export default ({className,...props,children}) => {
    const classname = classnames({
        "tag_btn": true
    }, className);
    return (
        <Button className={classname} {...props}>
            {children}
        </Button>
    );
};
