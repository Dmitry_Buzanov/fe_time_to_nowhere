// @flow
import React from "react";
import { Link } from "react-router-dom";
import Icon from "../../assets/Icon";
import { HeaderWrapper } from "./style/index";
import * as i from "../../assets/Icon/constants";

export default (props): React.Node | null => {
    return(
      <HeaderWrapper className="header-wrapper">
        <div className="header-create">
          <Link to="/edit">
              <Icon type={i.PENCIL} />
          </Link>
        </div>
        <div className="header-content">
          <div className="content-title">
            {props.title}
          </div>
          <img className="content-image" src={props.image} alt={props.name}></img>
          <div className="content-userinfo">
            {props.name}
            <div className="user-birth">
              <span> Date of birth: </span>
              <span> {props.date} </span>
            </div>
          </div>
          <div className="content-position">
            {props.position}
          </div>
        </div>
      </HeaderWrapper>
    );
}
