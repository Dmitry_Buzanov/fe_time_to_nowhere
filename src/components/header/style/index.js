import styled from "styled-components";

const position_color = '#51D2B3';
const title_color = '#393933';

export const HeaderWrapper = styled.div`
    background-color: #fff;
    width: 100%;
    height: 271px;
    position: fixed;
    top: 0;
    display: flex;
    justify-content: center;
    align-items: baseline;
    padding-top: 5px;
    box-sizing: border-box;
    border-bottom: 5px solid #E4E4E4;
    z-index: 1;

.header-create{
    position: absolute;
    top: 17px; right: 17px;
    svg{
        fill: #51D2B3;
    }
}
.header-content{
    font-family: Muli;
    height: 180px;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
}
.content-title{
    width: 100%;
    font-weight: bold;
    font-size: 36px;
    margin-bottom: 40px;
    color: ${title_color};
}
.content-userinfo{
    margin: 0 10%;
    font-size: 36px;
    display: flex;
    flex-direction: column;
    align-items: center;
    flex-wrap: wrap;
    span{
        font-size: 18px;
    }
}
.user-birth{
    width: 100%;
    display: flex;
    justify-content: space-between;
}
.content-position{
    font-weight: 600;
    font-size: 24px;
    color: ${position_color};
    width: 200px;
}
.content-image{
    width: 90px; height: 90px;
    border-radius: 50%;
}


`;