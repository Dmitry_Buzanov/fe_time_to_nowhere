import React from 'react';;
import CheckBox from './../../forms/Checkbox';
import { Wrapper } from './styles/';

export default  (props) => {
    const {onClick, isSelected, taskText } = props;
    return (
    <Wrapper>
        <CheckBox onClick={onClick}  isSelected={isSelected} /> <div>{taskText}</div>
    </Wrapper>
    )
}