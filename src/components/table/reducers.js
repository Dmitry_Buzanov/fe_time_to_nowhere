import {
    IS_SELECTED_TASK,
    SORTING_TASKS_BY_DEADLINE,
    REMOVE_SELECTED_TASKS,
    SORTING_TASKS_BY_START_DATE,
    FETCH_SUCCESS,
    SET_STATE, REMOVE_CURRENT_TASK,LOAD_DATA_SUCCEEDED,
} from './constants';

const INITAL_STATE = {
    tasksTitles: [],
    tasksRows: []
};

export default (state = INITAL_STATE, {type, isSelected, id, tasks, payload}) => {
    switch (type) {
        case IS_SELECTED_TASK:
            return {
                ...state,
                tasksRows: state.tasksRows.map((item) => {
                    if (item.id === id.toString()) {
                        item.isSelected = !item.isSelected
                    }
                    return item;
                }),
            };
        case REMOVE_SELECTED_TASKS:
            return {
                ...state,
                tasksRows: state.tasksRows.filter((item) => item.isSelected !== true)
            };

        case REMOVE_CURRENT_TASK:
            return {
                ...state,
                tasksRows: state.tasksRows.filter((item) => item.id !== id)
            };

        case SORTING_TASKS_BY_DEADLINE:
            return {
                ...state,
                tasksRows: state.tasksRows.sort((a, b) => new Date(b.deadline) - new Date(a.deadline))
            };

        case SORTING_TASKS_BY_START_DATE:
            return {
                ...state,
                tasksRows: state.tasksRows.sort((a, b) => new Date(b.start_date) - new Date(a.start_date))
            };
        case FETCH_SUCCESS:
            return {
                ...state,
                tasksList: tasks
            };
        case SET_STATE:
            return {
                ...tasks
            };
        case LOAD_DATA_SUCCEEDED: {
            return { ...payload };
        }
        default:
            return state
    }
};