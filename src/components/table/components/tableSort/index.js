import React from 'react';
import {IconWrapper} from './style'
import Icon from '../../../../../src/assets/Icon/index';
import * as i from "../../../../../src/assets/Icon/constants";


const TableSort = ({onSort}) => (
    <IconWrapper onClick={onSort}>
        <Icon type={i.SORT}/>
    </IconWrapper>
);

export default TableSort