import styled from "styled-components";

export const IconWrapper = styled.div`
    margin-left: 10px;
    cursor: pointer;
    &.rotate{
    transform: rotate(20deg);
    }
`;