import styled from "styled-components";

export const BtnWrapper = styled.div`
    position: absolute;
    right: -110px;
    top:20px;
    transition: 0.7s;
    opacity ${props => props.visible ? "1.0" : "0"};
    
    a{
    text-decoration: none;
    color: #F4F4F4;
    }
`;