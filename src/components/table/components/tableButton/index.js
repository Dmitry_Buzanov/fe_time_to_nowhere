import React from 'react';
import InfinityBtn from "../../../buttons/infinityBtn/index";
import DefaultBtn from "../../../buttons/defaultBtn/index";
import PlusBtn from "../../../buttons/plusBtn";
import * as i from "../../../../../src/assets/Icon/constants";
import {BtnWrapper} from "./style/index";
import {Link} from "react-router-dom";


const TableButton = ({type, onRemove, selected, id}) => {
        switch (type) {
            case "home":
                if (!selected) {
                    return null;
                } else {
                    return <BtnWrapper visible> <InfinityBtn leftBtnOnClick={() => onRemove(id)} leftBtnIco={i.BASKET}
                                                             rightBtnIco={i.SHARE}/></BtnWrapper>;
                }
            case "mentors":
                if (!selected) {
                    return null;
                } else {
                    return <BtnWrapper visible> <DefaultBtn>Ask a teach</DefaultBtn></BtnWrapper>;
                }
            case "students":
                if (!selected) {
                    return null;
                } else {
                    return <BtnWrapper visible>

                            <DefaultBtn><Link to={`/addTask`} className="buttonWrapper">Give a task</Link></DefaultBtn>

                    </BtnWrapper>;
                }
            case "learning":
                if (!selected) {
                    return null;
                } else {
                    return <BtnWrapper visible> <PlusBtn/></BtnWrapper>;
                }

            default:
                return null
        }
    }


;

export default TableButton