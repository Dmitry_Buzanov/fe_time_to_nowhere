import styled from "styled-components";

export const Cell = styled.div`
    height 70px;
    display: flex;
    text-decoration: none;
        color: #393939;
    align-items: center;
    justify-content: ${props => props.status ? "flex-end" : "flex-start"};
    max-width: ${props => props.status ? "90px" : "auto"};
    overflow:hidden;
`;