import React from 'react';
import Status from '../../status/index';
import {Cell} from './style';
import TableImg from "../../components/tableImg/index";


const CellValue = ({value, url, index}) => {

        if (value === "unfinished" || value === "finished" || value === "in process") {
            return <Cell status>
                {value}
                <Status text={value}/>
            </Cell>
        } else {
            return <Cell>
                <TableImg url={url} index={index}/>
                {value}
            </Cell>
        }
    }


;

export default CellValue