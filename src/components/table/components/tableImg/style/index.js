import styled from "styled-components";

export const Image = styled.img`
    width: 48px;
    height: 48px;
    border-radius: 50%;
    margin-right: 40px
    }
`;