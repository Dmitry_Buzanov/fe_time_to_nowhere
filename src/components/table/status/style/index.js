import styled from "styled-components";

const circle_red = "#D25151";
const circle_green = "#51D2B3";
const circle_yellow = "#D2C551";


export const Circle = styled.div`
    width: 13px;
    height: 13px;
    border-radius: 50%;
    background: ${props => props.red ? `${circle_red}` : props.green ? `${circle_green}` : `${circle_yellow}`};
    margin-left: 10px;
`;
