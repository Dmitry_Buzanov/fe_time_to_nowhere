import React from 'react';
import {Circle} from './style/index';

const Status = (props) => {
    if (props.text === "unfinished") {
        return <Circle red/>
    }
    else if (props.text === "finished") {
        return <Circle green/>
    }
    else if (props.text === "in process") {
        return <Circle yellow/>
    }
    else {
        return null
    }

}


export default Status