import React, {Component} from 'react';
import {MentorsTable, TableWrapper, TitleCell} from './style';
import Checkbox from '../../../forms/Checkbox';
import TableSort from '../../components/tableSort/index';
import TableButton from '../../components/tableButton/index';
import CellValue from '../../components/cellValue/index';
import ReactTable from "react-table";
import {Link} from "react-router-dom";

class Table extends Component {

    doAccessor = (item) => (
        item.replace(/\s+/g, '_').toLowerCase()
    );

    checkIsCellSort = (value, index) => {
        if (value === index) {
            return <TableSort key={index}/>
        }
        return null
    };

    renderSortIcon = (sortColumns, index) => (sortColumns.map((value) => {
        return this.checkIsCellSort(value, index)
    }));

    renderCellContent = (props, index) => {
        if (this.props.type === 'home'){
            return (
                <React.Fragment>
                    <Link to="/home/task">
                        <CellValue value={props.value} url={props.original.img} index={index}/>
                    </Link>
                </React.Fragment>
            )
        }else {
            return (
                <React.Fragment>
                        <CellValue value={props.value} url={props.original.img} index={index}/>
                </React.Fragment>
            )
        }

    };

    renderColumns = (arr, sortColumns) => {
        return arr.map((item, index) => (
            {

                Header: <TitleCell>{item} {this.renderSortIcon(sortColumns, index)} </TitleCell>,
                accessor: this.doAccessor(item),
                Cell: props =>
                    <React.Fragment>
                        <Checkbox onClick={this.props.isSelectedTask} isSelected={props.original.isSelected}
                                  id={props.original.id}/>
                        <TableButton type={this.props.type} onRemove={this.props.removeCurrentTask}
                            selected={props.original.isSelected} id={props.original.id}/>
                        {this.renderCellContent(props, index)}
                    </React.Fragment>,
            }
        ))
    };

    mapTitle = (arr, sortColumns) => {
        return [{
            Header: null,
            columns: this.renderColumns(arr, sortColumns)
        }]
    };
    render() {
        const {tasks, sortColumns} = this.props;
        if (tasks.tasksRows) {
            return (
                <TableWrapper>
                    <MentorsTable>
                        <ReactTable
                            columns={this.mapTitle(tasks.tasksTitles, sortColumns)}
                            data={tasks.tasksRows}
                            resizable={true}
                            showPaginationBottom={false}
                            loading={false}
                            minRows={0}
                            loadingText={''}
                        />
                    </MentorsTable>
                </TableWrapper>
            )
        } else return null
    }
}
export default Table
