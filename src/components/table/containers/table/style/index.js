import styled from "styled-components";

export const MentorsTable = styled.div`
    width: 100%;
    box-sizing: border-box;
    border-collapse: collapse;
    margin-top: 75px;
    .-headerGroups{
    display: none;
    }
`;
export const TableWrapper = styled.div`
    margin: 0 auto;
    width: 100%;
    max-width: 1300px;
    box-sizing: border-box;
    .ReactTable{
    width:100%;
    }
    > div {
    display: flex;
    font-family: Muli;
    font-style: normal;
    font-weight: normal;
    line-height: normal;
    font-size: 14px;
    color: #393939;
    box-sizing: border-box;
    padding-left: 9%;
    border-bottom: 0.5px solid rgba(113, 113, 113, 0.5);
    position: relative;
    
    .rt-resizer{
    display: inline-block;
    position: absolute;
    width: 36px;
    top: 0;
    bottom: 0;
    right: 0;
    cursor: col-resize;
    z-index: 10;
    }}
    .rt-tr{
    display: flex;
    font-family: Muli;
    font-style: normal;
    font-weight: normal;
    line-height: normal;
    font-size: 14px;
    color: #393939;
    box-sizing: border-box;
    padding-left: 9%;
    border-bottom: 0.5px solid rgba(113, 113, 113, 0.5);
    position: relative;
    }
    
    .rt-resizer{
    display: inline-block;
    position: absolute;
    width: 36px;
    top: 0;
    bottom: 0;
    right: 0;
    cursor: col-resize;
    z-index: 10;
    }
    .rt-th{
    position: relative;
    outline: none
    }
`;

export const TableContentRow = styled.div`
    
    
    
`;

export const TitleCell = styled.div`
    display: flex;
    padding: 0 0 20px 0;
    box-sizing: border-box;
    font-family: Muli;
    font-style: normal;
    font-weight: bold;
    line-height: normal;
    font-size: 14px;   
    overflow:hidden;
`;