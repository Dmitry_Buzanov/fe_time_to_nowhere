import Component from "../containers/table/index";
import {
    compose,
    setDisplayName,
    defaultProps,
    lifecycle,
} from 'recompose';
import {connect} from 'react-redux';
import {
    isSelectedTask,
    removeSelectedTasks,
    removeCurrentTask,
    fetchTask,
    fetch,
    setState
} from "../actions";
import addReducer from "./../../../HOComponents/addReducer";
import {NAME} from "../constants";
import reducers from "../reducers";
import addSaga from "../../../HOComponents/addSaga/index";
import sagas from "../sagas/index";


export default compose(
    setDisplayName(`components/as`),
    connect(state => ({
        tasks: state[NAME],
    }), {isSelectedTask, removeSelectedTasks, removeCurrentTask, fetchTask, fetch, setState}),
    addSaga({sagas}),
    addReducer([NAME], reducers),
    lifecycle({
        componentWillMount(){
            this.props.setState(this.props.data);
        },
    }),
    defaultProps({
        state: {}
    }),
)(Component)