import {
    IS_SELECTED_TASK,
    REMOVE_SELECTED_TASKS,
    SORTING_TASKS_BY_DEADLINE,
    SORTING_TASKS_BY_START_DATE,
    FETCH_SUCCESS,
    FETCH,
    SET_STATE,REMOVE_CURRENT_TASK
} from './constants';


export const setState = (tasks) => ({
    type: SET_STATE,
    tasks,
});
export const isSelectedTask = (id) => ({
    type: IS_SELECTED_TASK,
    id
});
export const removeSelectedTasks = () => ({
    type: REMOVE_SELECTED_TASKS,
});

export const removeCurrentTask = (id) => ({
    type: REMOVE_CURRENT_TASK,
    id
});

export const sortingTasksByDeadline = () => ({
    type: SORTING_TASKS_BY_DEADLINE,

});
export const sortingTasksByStartDate = () => ({
    type: SORTING_TASKS_BY_START_DATE,

});
export const fetchTask = (tasks) => ({
    type: FETCH_SUCCESS,
    tasks
});
export const fetch = () => ({
    type: FETCH,

});