// @flow

import { NAME } from '../constants';
import watcherSearchData from './tableData';

export default {
    [`${NAME}-watcherSendForm`]: watcherSearchData,

};
  