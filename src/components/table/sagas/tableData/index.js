import {call, put, takeEvery} from 'redux-saga/effects';
import fetch from '../../../../api/index';
import {FETCH_SUCCESS, FETCH_FAILED, FETCH} from "../../constants";

/*import * as contextT from '../../../../features/context/actionTypes'*/

export function* tableData(action: any): any {

    try {
        const response = yield call(fetch.get, '/home/mentors');

        yield put({
            type: FETCH_SUCCESS,
            tasks: response.data
        });
    } catch (error) {
        const response = error.response || {data: {}};
        yield put({
            type: FETCH_FAILED,
            error: {
                message: response.data.message || error.message,
                stack: error.stack,
                status: error.response && error.response.status,
                statusText: error.response && error.response.statusText
            }
        });
    }
}

export default function* watcherTableData(): any {
    yield takeEvery(FETCH, tableData);
}