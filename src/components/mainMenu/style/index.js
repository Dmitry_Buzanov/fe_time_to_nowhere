import styled from "styled-components";

export const MenuTopContainer = styled.div`
    position: fixed;
    top: 270px;
    width: 100%;
    height: 70px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.05);
    padding: 0 20%;
    box-sizing: border-box;
    z-index: 1;
    background-color: #fff;
`;
