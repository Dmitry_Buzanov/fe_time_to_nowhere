import styled from "styled-components";
import { NavLink } from "react-router-dom";
const main_color = "#000000";
const active_color = "#51D2B3";

export const MenuTopItem = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  /* &::before {
    z-index: 1;
    box-shadow: none;
  } */
`;

export const StyledNavLink = styled(NavLink)`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
  font-family: Muli;
  font-size: 18px;
  font-weight: 600;
  color: ${main_color};
  text-decoration: none;

  &.active-item {
    position: relative;
    background: #ffffff;
    color: ${active_color} !important;
  }
  &.active-item::before,
  &.active-item::after {
    content: "";
    position: absolute;
    background: #ffffff;
    left: 50%;
    bottom: -26px;
    width: 30px;
    height: 30px;
    box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.05);
    z-index: -1;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg) translateX(-50%);
    cursor: default;
  }
  &::before {
    z-index: 1;
    box-shadow: none;
  }
`;
