import React from "react";
import { MenuTopItem, StyledNavLink } from "./style/index";

export default (props): React.Node | null => {
  return (
    <MenuTopItem>
      <StyledNavLink to={props.endPoint} activeClassName={"active-item"}>
        {props.children}
      </StyledNavLink>
    </MenuTopItem>
  );
};
