export const ItemsArray = [
    {
        endPoint:"/home/tasks",
        content: 'My tasks',
    },
    {
        endPoint:"/home/mentors",
        content: 'Mentors',
    },
    {
        endPoint:"/home/students",
        content: 'My students',
    },
    {
        endPoint:"/home/progress",
        content: 'My progress',
    },
    {
        endPoint:"/home/cources",
        content: 'My cources',
    },
    {
        endPoint:"/home/knowledge",
        content: 'Knowledge spheres',
    },
];
