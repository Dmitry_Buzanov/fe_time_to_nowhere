// @flow
import React from "react";
import  MenuItem  from './MenuItem';
import { ItemsArray } from './content';
import { MenuTopContainer } from './style/index'

export default (props): React.Node | null => {

    const menuItemsArray = ItemsArray.map((item, index)=>(
      <MenuItem classNames={"menu-top-item"} 
                endPoint={item.endPoint}
                key={index}>{item.content}
      </MenuItem>
  ));

    return(
      <MenuTopContainer className="menu-top-container">
            {menuItemsArray}
      </MenuTopContainer>
    );
}
