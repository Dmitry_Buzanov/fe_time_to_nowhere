// @flow
import * as React from "react";
import  CloseButton  from './../buttons/closeButton';

import {
  Popup,
  Background,
  ModalClose,
  ModalContent,
  ModalTitle,
  ModalText
} from "./style/index";

export default (props): React.Node | null => {
  return (
    <div>
      <Background className="background-blur">
        <div className="background-filter" />
      </Background>
      <Popup className="modal">
        <ModalContent className="modal-content">
          <ModalClose className="modal-close">
            <CloseButton onClick={props.onClick} />
          </ModalClose>
          <ModalTitle className="modal-title">{props.title}</ModalTitle>
          <ModalText className="modal-text">{props.message}</ModalText>
        </ModalContent>
      </Popup>
    </div>
  );
};