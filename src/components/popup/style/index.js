import styled from "styled-components";
const title_color = '#51D2B3';
const text_color = '#393933';

export const Popup = styled.div`
  &.modal {
    width: 370px;
    padding: 30px 35px;
    border-radius: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    -webkit-box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.1);
    box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.1);
    background: #ffffff;
    z-index: 9999;
  }
`;
export const Background = styled.div`
  &.background-blur {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.9);
    z-index: 99;
  }
`;

export const ModalTitle = styled.div`
  max-width:253px;
  font-family: Muli, sans-serif;
  font-size: 36px;
  font-weight: 600;
  color: ${title_color};
  line-height: 36px;
`;

export const ModalText = styled.div`
  display: inline-block;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 100%;
  font-family: Muli, sans-serif;
  font-size: 14px;
  font-weight: 600;
  color: ${text_color};
  padding: 30px 0 20px 0;
  text-align: center;
`;
export const ModalContent = styled.div`
  &.modal-content {
    position: relative;
  }
`;
export const ModalClose = styled.div`
  &.modal-close {
    position: absolute;
    top: 0;
    right: 0;
  }
`;
