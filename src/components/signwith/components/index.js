// @flow
import * as React from "react";
// import type { Props } from "./../types";
import { Redirect } from "react-router-dom";
// import "./style/style.css";
import { SignWith } from './style/index';
import { GoogleLogin } from "react-google-login";
import googleIcon from "./icons/google-icon.svg";
import facebookIcon from "./icons/facebook_logo.svg";
import crowdIcon from "./icons/crowd_logo.svg";

export default (props): React.Node | null => {
  const { pageData } = props;
  const { sendGoogleData } = props.actions;
  const { from } = props.location.state || {};
  const responseGoogle = response => {
    if (response.profileObj) {
      const googleUserData = {
        email: response.profileObj.email
      };
      sendGoogleData(googleUserData);
    }
  };
  if (pageData && pageData.token) {
    return <Redirect to={from || { pathname: "/" }} />;
  }
  return (
    <div className="wrapped-root">
      <SignWith className="sign_with">
        <div className="wrapped_sign_with">
          <div className="sign_with_text">sign in with:</div>
          <div className="sign_with_icons">
            <div>
              <img
                className="crowdIcon"
                src={crowdIcon}
                alt="Sign in with crowd"
              />
            </div>
            <div>
              <img
                className="facebookIcon"
                src={facebookIcon}
                alt="Sign in with facebook"
              />
            </div>
            <div>
              <GoogleLogin
                clientId="597480837373-6uaoqt7ph3l78f8cj88r7ot1gr5kc8or.apps.googleusercontent.com"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                className="googleIcon"
              >
                <img src={googleIcon} alt="Sign in with google" />
              </GoogleLogin>
            </div>
          </div>
        </div>
      </SignWith>
    </div>
  );
};
