import styled from "styled-components";

export const SignWith = styled.div`
  &.sign_with {
    position: absolute;
    bottom: 58px;
    left: 50%;
    transform: translateX(-50%);
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  &.wrapped_sign_with {
    width: 150px;
    display: flex;
    flex-direction: column;
  }
  &.sign_with .sign_with_text {
    font-family: "Myli", sans-serif;
    font-style: normal;
    font-weight: bold;
    line-height: normal;
    font-size: 24px;
    text-align: center;
  }
  &.sign_with .sign_with_icons {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top: 8px;
    margin-left: 7px;
    margin-right: 7px;
  }
  &.googleIcon {
    padding: 0;
    background: none;
    border: none;
    cursor: pointer;
  }
  &.crowdIcon {
    cursor: pointer;
  }
  &.facebookIcon {
    cursor: pointer;
  }
`;
