// @flow

import * as t from './actionTypes';

export const sendForm = (data: any = {}) => ({
    type: t.SIGNIN,
    payload: data
});

export const sendGoogleData = (token: any = {}) => ({
    type: t.SIGNIN_WITH_GOOGLE,
    payload: token
});

export const updateFormState = (form, state) => ({
    type: t.UPDATE_FORM_STATE,
    form,
    payload: state
  })

export const refresh = () => ({
    type: t.REFRESH
});