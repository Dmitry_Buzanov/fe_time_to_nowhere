import * as t from './../../actionTypes';
import { call, put, takeEvery } from 'redux-saga/effects';
import fetch from './../../../../api';
import * as contextT from '../../../../features/context/actionTypes'
export function* sendGoogleData(action: any): any {
  try {
    const response = yield call(fetch.post, '/auth/google/signin', action.payload);
    const jwtAuth =  {
      token: response.data.token,
      refreshToken: response.data.refreshToken
    };
    localStorage.setItem("jwtAuth", JSON.stringify(jwtAuth));
    yield put({
      type: contextT.GET_USER_DATA,
      payload: {email:response}
    });    
    yield put({
      type: contextT.ADD_CONTEXT,
      payload: response.data
    });
    yield put({
      type: t.SIGNIN_SUCCEEDED,
      payload: { ...response, user_id: action.payload.username }
    });
  } catch (error) {
    const response = error.response || { data: {} };
    yield put({
      type: t.SIGNIN_FAILED,
      error: {
        message: response.data.message || error.message,
        stack: error.stack,
        status: error.response && error.response.status,
        statusText: error.response && error.response.statusText
      }
    });
  }
}

export default function* watcherSendForm(): any {
  yield takeEvery(t.SIGNIN_WITH_GOOGLE, sendGoogleData);
}