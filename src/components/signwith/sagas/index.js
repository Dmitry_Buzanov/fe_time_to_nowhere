// @flow

import { NAME } from './../constants';
import watcherSendGoogleData from './sendGoogleData';
export default {
    [`${NAME}-watcherSendGoogleData`]: watcherSendGoogleData,
};
  