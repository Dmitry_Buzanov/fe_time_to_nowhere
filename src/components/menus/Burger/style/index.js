import styled from "styled-components";
const background_btn = "#51D2B3";
const btn_color = "#EFEFEF";

export const MenuButton = styled.div`
  &.menu_btn {
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${background_btn};
    height: 70px;
    width: 70px;
    flex-direction: column;
    cursor: pointer;
  }

  &.menu_btn span {
    display: block;
    height: 4px;
    width: 29px;
    background-color: ${btn_color};
    border-radius: 20px;
    transition: 0.3s;
  }

  &.menu_btn span:nth-child(2) {
    margin: 5px 0;
  }

  &.menu_btn&.active span:nth-child(1) {
    transform: rotate(-45deg);
    transform-origin: left center;
    margin-bottom: -3px;
    width: 27px;
  }

  &.menu_btn&.active span:nth-child(2) {
    transform: translatex(30px);
    height: 0;
    margin: 0;
    opacity: 0;
  }
  
  &.menu_btn&.active span:nth-child(3) {
    transform: rotate(45deg);
    transform-origin: left center;
    margin-top: -3px;
    width: 27px;
  }
`;
