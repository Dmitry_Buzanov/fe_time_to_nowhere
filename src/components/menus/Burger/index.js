import React from 'react';
import { MenuButton } from './style/index' 

export const BurgerButton = ({isActive, onClick})=>{
    let classNames = "menu_btn";
    if(isActive){
        classNames = "menu_btn active";
    }

    return(
        <MenuButton className={classNames} onClick={onClick}>
            <span></span>
            <span></span>
            <span></span>
        </MenuButton>
    );
}