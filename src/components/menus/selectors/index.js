import { createSelector } from 'reselect';
import { NAME } from '../constants';

export const getData = (state) => state[NAME].isOpened;
export const getElement = (state) => state[NAME].hoveredElement;
export const getFocus = (state) => state[NAME].isFocused;

export const selectData = createSelector(getData, (isOpened) => {
  return isOpened;
});

export const selectElement = createSelector(getElement, (hoveredElement) => {
  return hoveredElement;
});

export const selectFocus = createSelector(getFocus, (isFocused) => {
  return isFocused;
});