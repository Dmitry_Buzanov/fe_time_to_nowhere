import { connect } from 'react-redux';
import {selectData, selectElement, selectFocus} from "../selectors"
import * as actionCreators from './../actions';
import { bindActionCreators } from 'redux';
import addReducer from "../../../HOComponents/addReducer";
import addSaga from "../../../HOComponents/addSaga";
import { NAME } from '../constants';
import reducers from "../reducers";
import { compose,setDisplayName,withHandlers } from 'recompose'
import sagas from "../sagas";
import Component from "./components";

const mapStateToProps = (state) =>({
    isOpened: selectData(state),
    hoveredElement: selectElement(state),
    isFocused: selectFocus(state)
});

const MapDispatchToProps = (dispatch) =>({
    actions: bindActionCreators(actionCreators,dispatch)
});

export default compose(
    setDisplayName(`components/${NAME}`),
    connect(mapStateToProps,MapDispatchToProps),
    addReducer(NAME,reducers),
    addSaga({sagas}),
    withHandlers({
        toggleMenu: props => (values: any) => () => {
          return props.actions.toggleMenu();
        },
        openMenu:  props => (values: any) => () => {
            return props.actions.openMenu();
        },
        closeMenu:  props => (values: any) => () => {
            return props.actions.closeMenu();
        },
        hoverElem: props => (values: any) => (hoveredElement) => {
            return props.actions.hoverElem(hoveredElement);
        },
        leaveElem: props => (values: any) => () => {
            return props.actions.leaveElem();
        },
        setFocus: props => (values: any) => () => {
            return props.actions.setFocus();
        },
        unFocus: props => (values: any) => () => {
            return props.actions.unFocus();
        },
      }),
)(Component);
