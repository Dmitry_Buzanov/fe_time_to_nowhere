import styled from "styled-components";

const menu_background = '#393939';
const name_color = '#51D2B3';

export const SideContainer = styled.div`
&.side_container{
    position: fixed;
    z-index: 3000;
    top: 0;
    left: 0;
}
.side_menu__burger{
    position: absolute;
    top: 0;
    left: 0;
    width: 70px;
}
.side_menu{
    position: relative;
    z-index: 3002;
    background: ${menu_background};
    left: 0;
    top: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 70px;
    height: 100vh;
}
.side_menu__container{
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
}
.side_slider_menu{
    display: block;
    z-index: 3001;
    position: absolute;
    top: 0;
    left: -220px;
    background: #393939;
    color: #717171;
    transition: .3s;
}
.side_slider_menu__wrapper{
    position: relative;
    width: 290px;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
}
.side_slider_menu__name{
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 70px;
    font-family: "Muli";
    font-style: normal;
    font-weight: 800;
    line-height: normal;
    font-size: 36px;
    color: ${name_color};
}
.side_slider_menu.open{
    left: 70px;
}

`;