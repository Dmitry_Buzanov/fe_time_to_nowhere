import * as React from "react";
import {ItemsArray} from "../../content";
import SearchInput from "../../../forms/SearchInput";
import {BurgerButton} from "../../Burger";
import {MenuItem} from "../../Items";
import {SideContainer} from "./style/index";

export default (props = {}): React.Node | null => {
    let classNames = "side_slider_menu";

    if (props.isOpened) {
        classNames = "side_slider_menu open";
    }
    const menuItemsArray = ItemsArray.map((item, index) => (
        <MenuItem onMouseLeave={props.actions.leaveElem} onMouseEnter={() => props.actions.hoverElem(index)}
                  endPoint={item.endPoint} key={index} onClick={item.onClick ? props.actions.toggleMenu : props.actions.closeMenu}>
            <div
                className={props.hoveredElement !== null && props.hoveredElement === index ? "menu_item_icon hovered" : "menu_item_icon"}>{item.icon}</div>
        </MenuItem>
    ));
    const slideMenuItemsArray = ItemsArray.map((item, index) => (
        <MenuItem onMouseLeave={props.actions.leaveElem} onMouseEnter={() => props.actions.hoverElem(index)}
                  endPoint={item.subcontent.endPoint} key={index}>
            {item.subcontent.isComponent ?
                <div className="menu_item_content">
                    <SearchInput isFocused={props.isFocused} onBlur={props.actions.unFocus}/>
                </div>
                :
                <div
                    className={props.hoveredElement !== null && props.hoveredElement === index ? "menu_item_content hovered" : "menu_item_content"} onClick={props.actions.closeMenu}>
                    {item.subcontent.content}
                </div>
            }
        </MenuItem>
    ));
    return (
        <SideContainer className="side_container">
            <div className="side_menu">
                <div className="side_menu__burger">
                    <BurgerButton isActive={props.isOpened} onClick={props.actions.toggleMenu}/>
                </div>
                <div className="side_menu__container">
                    {menuItemsArray}
                </div>
            </div>
            <div className={classNames}>
                <div className="side_slider_menu__wrapper">
                    <div className="side_slider_menu__name">
                        menu
                    </div>
                    <div className="side_menu__container">
                        {slideMenuItemsArray}
                    </div>
                </div>
            </div>
        </SideContainer>
    )
}