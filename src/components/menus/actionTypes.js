import { NAME } from './constants';

export const TOGGLE = `${NAME}/TOGGLE`;
export const OPEN = `${NAME}/OPEN`
export const HOVERED = `${NAME}/HOVERED`;
export const LEAVED = `${NAME}/LEAVED`;
export const FOCUS = `${NAME}/FOCUS`;
export const UNFOCUS = `${NAME}/UNFOCUS`;
export const ICONTAP = `${NAME}/ICONTAP`;
export const CLOSE = `${NAME}/CLOSE`;
