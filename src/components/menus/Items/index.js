import React from 'react';
import {NavLink} from "react-router-dom";
import { Item } from "./styles/index";


export const MenuItem = (props) =>{
    return(
        <Item className="menu_item" onMouseEnter={props.onMouseEnter} onMouseLeave={props.onMouseLeave} onClick={props.onClick}>
            { props.endPoint?<NavLink to={props.endPoint} activeClassName="selected">{props.children}</NavLink>:props.children }            
        </Item>
    )
} 