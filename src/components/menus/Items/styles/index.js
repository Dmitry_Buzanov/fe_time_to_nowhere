import styled from "styled-components";

const main_color = '#717171';
const active_color = '#E5E5E5';
const paddingLeft = '50px';
const menuItemWidth = 'calc(100% - 50px)';


export const Item = styled.div`
&.menu_item{
    height: 80px;
    width: 100%;
    cursor: pointer;
}
&.menu_item a{
    display: block;
    width: 100%;
    height: 100%;
}
.menu_item_icon{
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
}
.menu_item_icon>svg{
    width: 28px;
    height: 28px;
    fill: ${main_color};
    transition: .3s;
}
.menu_item_icon.hovered>svg{
    fill: ${active_color};
}
.menu_item_content{
    display: flex;
    width: ${menuItemWidth};
    height: 100%;
    justify-content: left;
    align-items: center;
    padding-left: ${paddingLeft};
    color: ${main_color};
    font-family: Muli;
    font-style: normal;
    font-weight: bold;
    line-height: normal;
    font-size: 14px;
    transition: .3s;
}
.menu_item_content.hovered{
    color: ${active_color};
}
.menu_item a.selected>.menu_item_content{
    color: ${active_color};
}
.menu_item a.selected>.menu_item_icon>svg{
    fill: ${active_color};
}


`;