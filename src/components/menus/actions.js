import * as t from './actionTypes';

export const toggleMenu = () => {
    return{
    type: t.TOGGLE,
    }
};

export const openMenu = () => {
    return{
    type: t.ICONTAP,
    }
};
export const closeMenu = () => {
    return{
    type: t.CLOSE,
    }
};

export const hoverElem = (hoveredElement) => {
    return{
    type: t.HOVERED,
    hoveredElement: hoveredElement
    }
};

export const leaveElem = () => {
    return{
    type: t.LEAVED
    }
};

export const setFocus = () =>{
    return {
        type: t.FOCUS
    }
}

export const unFocus = () =>{
    return {
        type: t.UNFOCUS
    }
}
