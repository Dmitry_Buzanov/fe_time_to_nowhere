import React from "react";
import Icon from "../../assets/Icon";
import * as i from "../../assets/Icon/constants";

export const ItemsArray = [
  {
    endPoint: "/home",
    icon: <Icon type={i.HOME} />,
    subcontent: {
      endPoint: "/home",
      content: "Home"
    }
  },
  {
    endPoint: "/learning",
    icon: <Icon type={i.LEARN} />,
    subcontent: {
      endPoint: "/learning",
      content: "Learning Base"
    }
  },
  {
    endPoint: "/edit",
    icon: <Icon type={i.SETTINGS} />,
    subcontent: {
      endPoint: "/edit",
      content: "edit"
    }
  },
  {
    endPoint: "/auth/signout",
    icon: <Icon type={i.LOGOUT} />,
    subcontent: {
      endPoint: "/auth/signout",
      content: "Log Out"
    }
  }
];
