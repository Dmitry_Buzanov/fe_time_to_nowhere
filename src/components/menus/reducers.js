import * as t from "./actionTypes";

const initialState = {
    isOpened: false,
    hoveredElement: null,
    isFocused: false
};

export default (state = initialState,action) =>{
    switch(action.type){
        case t.TOGGLE: {
            return {...state, isOpened: !state.isOpened};
        }
        case t.OPEN:{
            return {...state,isOpened:true};
        }
        case t.CLOSE:{
            return {...state,isOpened:false};
        }
        case t.HOVERED: {
            return {...state,hoveredElement: action.hoveredElement}
        }
        case t.LEAVED: {
            return {...state,hoveredElement: null}
        }
        case t.FOCUS: {
            return {...state,isFocused: true}
        }
        case t.UNFOCUS: {
            return {...state,isFocused: false}
        }
        default:
            return state;
    }
}