import * as t from '../../actionTypes';
import { put, takeEvery } from 'redux-saga/effects';

export function* hoverElement(action: any): any {
    yield put({
      type: t.HOVERED,
      hoveredElement: action.hoveredElement
    });
}

export function* watcherHoverElement(): any {
  yield takeEvery(t.HOVER, hoverElement);
}

export function* leaveElement(action: any): any {
    yield put({
      type: t.LEAVED,
    });
}

export function* watcherLeaveElement(): any {
  yield takeEvery(t.LEAVE, leaveElement);
}