import * as t from '../../actionTypes';
import { put, takeEvery } from 'redux-saga/effects';

export function* toggleMenu(action: any): any {
    yield put({
      type: t.TOGGLE,
    });
}

export default function* watcherToggleMenu(): any {
  yield takeEvery(t.MENU, toggleMenu);
}