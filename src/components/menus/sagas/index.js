import { NAME } from '../constants';

import watcherOpenMenu from './openMenu'

export default {
    [`${NAME}-watcheOpenMenu`]: watcherOpenMenu
};