import * as t from '../../actionTypes';
import {put, takeEvery} from 'redux-saga/effects';

export function* openMenu(action: any): any {
    yield put({
        type: t.OPEN
    });
    yield put({
        type: t.FOCUS
    });
}

export default function* watcherOpenMenu(): any {
    yield takeEvery(t.ICONTAP, openMenu);
}

