import fetch from "../../api";

async function refreshTokens(refreshToken) {
  try {
    localStorage.setItem("jwtAuth", JSON.stringify({
      token: refreshToken,
      refreshToken
    }));

    const response = await fetch.get("/auth/refresh/");
    localStorage.setItem("jwtAuth", JSON.stringify({
      token: response.data.token,
      refreshToken: response.data.refreshToken
    }));
  } catch (error) {
    console.log(error.response);
  }
}


const authChecker = JsonWebToken => store => next => async (action) => {
  const isTokenNeedRefresh = (
    JsonWebToken !== null
    && JsonWebToken.token
    && JsonWebToken.refreshToken
    && (action.error !== undefined )
    && (action.error.status === 401)
  );

  if (isTokenNeedRefresh) {
    await refreshTokens(JsonWebToken.refreshToken);
    next(action);
  }

  next(action);
};

export default authChecker;