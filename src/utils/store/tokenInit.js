const initToken = (store, action, JsonWebToken) => {
  const isTokenInLocalStorage = !!JsonWebToken;
  if(isTokenInLocalStorage) store.dispatch(action(JsonWebToken));
};

export default initToken;