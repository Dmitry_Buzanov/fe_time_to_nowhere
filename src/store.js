// @flow
import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { all, fork } from "redux-saga/effects";
import rootReducer from "./reducers";
import { sendContext } from "./features/context/actions/index";
import { createLogger } from "redux-logger";
import authChecker from "./utils/store/authCheck";
import tokenInit from "./utils/store/tokenInit";

const sagaMiddleware = createSagaMiddleware();

function configureStore(JsonWebToken) {
  let middleware;
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  if (process.env.NODE_ENV === "development") {
    middleware = composeEnhancers(
      applyMiddleware(sagaMiddleware, authChecker(JsonWebToken), createLogger())
    );
  } else {
    middleware = composeEnhancers(applyMiddleware(sagaMiddleware));
  }
  const store = createStore(rootReducer(), middleware);
  store.asyncReducers = {};
  tokenInit(store, sendContext, JsonWebToken);
  return store;
}

export function injectAsyncReducer(store, name, asyncReducer) {
  store.asyncReducers[name] = asyncReducer;
  store.replaceReducer(rootReducer(store.asyncReducers));
}

export function injectAsyncSaga(asyncSaga) {
  function* rootSaga() {
    yield all(
      Object.keys(asyncSaga).map(sagaName => fork(asyncSaga[sagaName]))
    );
  }
  sagaMiddleware.run(rootSaga).done.catch(error => console.warn(error));
}

const JsonWebToken = JSON.parse(localStorage.getItem("jwtAuth"));

export default configureStore(JsonWebToken);
