// @flow
import * as React from "react";
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import history from "./history";
import Auth from "./navigators/auth";
import Home from "./navigators/home";
import EditProfile from "./navigators/editProfile";
import Home2 from "../features/home";

export default (): React.Node => (
  <Router basename="/" history={history}>
    <Switch>
      <Redirect exact from="/" to="/home" />
      <Route path="/auth" component={Auth} />
      <Route path="/home" component={Home} />
      <Route path="/edit" component={EditProfile} />
      <Route exact path="/addTask" component={Home2} />
      {/*Test routes*/}
      <Route path="/learning" component={Home2} />
      <Route path="/settings" component={Home2} />
    </Switch>
  </Router>
);
