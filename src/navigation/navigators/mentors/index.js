// @flow
import * as React from 'react';

import { Switch} from 'react-router-dom';
import PrivateRoute from "./../../../components/routers/PrivateRoute";
import Mentors from "./../../../features/mentors/index";
export default (): React.Node => {
    return (
        <Switch>
            <PrivateRoute exact path="/home/mentors" component={Mentors} />
        </Switch>
    );
};