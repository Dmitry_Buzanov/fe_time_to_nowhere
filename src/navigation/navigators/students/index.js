// @flow
import * as React from 'react';
import { Switch } from 'react-router-dom';
import PrivateRoute from "./../../../components/routers/PrivateRoute";
import Students from "./../../../features/students";

export default (): React.Node => {
    return (
        <Switch>
            <PrivateRoute exact path="/home/students" component={Students} />
        </Switch>
    );
};