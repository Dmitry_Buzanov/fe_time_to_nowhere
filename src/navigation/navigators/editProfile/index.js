// @flow
import * as React from 'react';

import { Switch} from 'react-router-dom';
import PrivateRoute from "./../../../components/routers/PrivateRoute";
import EditProfile from "./../../../features/editProfile/index";
export default (): React.Node => {
    return (
        <Switch>
            <PrivateRoute exact path="/edit" component={EditProfile} />
        </Switch>
    );
};