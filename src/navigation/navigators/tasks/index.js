// @flow
import * as React from 'react';

import { Switch } from 'react-router-dom';
import PrivateRoute from "./../../../components/routers/PrivateRoute";
import Tasks from "./../../../features/tasks/index";
export default (): React.Node => {
    return (
        <Switch>
            <PrivateRoute exact path="/home/tasks" component={Tasks} />
        </Switch>
    );
};