import { combineReducers } from 'redux';
import { formReducers } from './formReducer';


export default function createReducer(asyncReducers = {}) {
  return combineReducers({
    form: formReducers,
    ...asyncReducers
  });
}