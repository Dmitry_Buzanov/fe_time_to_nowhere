import {addDecorator, configure} from '@storybook/react';
import {withBackgrounds} from "@storybook/addon-backgrounds";
import { GlobalStyle } from"../src/styles/index"

function loadStories() {
    addDecorator(
        withBackgrounds([
            { name: 'white', value: '#ffffff', default: true },
            { name: 'twitter', value: '#00aced' },
            { name: 'black', value: '#000000' }
        ])
    )
    require('../stories/index.js');

}

configure(loadStories, module);