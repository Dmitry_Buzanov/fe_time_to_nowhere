import React from "react";
import { storiesOf } from "@storybook/react";
import styled from "styled-components";
import { compose, withState } from "recompose";
import SphereCard from "../../src/components/cards/components/sphereCard/index";
import AddSphereCard from "../../src/components/cards/components/addSphereCard/index";
import ProgressCard from "../../src/components/cards/components/progressCard/index";
import SpherePopup from "../../src/components/cards/components/addSphereCard/components/popup/index";

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding: 20px;
`;

const tasks = [{
  title: "Javascript",
  tasksCompleted: 12,
  tasksToPromotion: 3,
  remainingTasks: [{
    text: "Прочитать Книгу Java Script начальный курс Frontend разработчика.",
    dateInfo: {
      startDate: "01.10.2019",
      endDate: "01.10.2019"
    }
  },
    {
      text: "Заверстать Landing Page по готовому макету.",
      dateInfo: {
        startDate: "01.10.2019",
        endDate: "01.10.2019"
      }
    },
    {
      text: "Подготовить 4 анимации появления ховеров и поп-апов на HTML5.",
      dateInfo: {
        startDate: "01.10.2019",
        endDate: "01.10.2019"
      }
    }]
},
  {
    title: "PHP",
    tasksCompleted: 5,
    tasksToPromotion: 10,
    remainingTasks: [{
      text: "Прочитать Книгу Java Script начальный курс Frontend разработчика.",
      dateInfo: {
        startDate: "01.10.2019",
        endDate: "01.10.2019"
      }
    },
      {
        text: " Заверстать Landing Page по готовому макету.",
        dateInfo: {
          startDate: "01.10.2019",
          endDate: "01.10.2019"
        }
      },
      {
        text: "Подготовить 4 анимации появления ховеров и поп-апов на HTML5.",
        dateInfo: {
          startDate: "01.10.2019",
          endDate: "01.10.2019"
        }
      }]
  },
  {
    title: "Java",
    tasksCompleted: 120,
    tasksToPromotion: null,
    remainingTasks: null
  }]
});

const PopupWithState = compose(
  withState("isOpen", "toggle", true)
)(({ isOpen, toggle }) => {
  return (
    <React.Fragment>
      <span onClick={()=>{toggle(!isOpen);}}>open popup</span>
      <div>{isOpen && <SpherePopup onClick={() => {toggle(!isOpen)}  }/>} </div>
    </React.Fragment>)
});

const Cards = storiesOf("Cards", module).add("SphereCard", () => (
  <Wrapper>
    <SphereCard
      userStatus="Mentor"
      sphere="JavaScript"
      level="Senior"
      tasksDone={60}
      tasksCreated={15}
      students={5}
    />
    <SphereCard
      userStatus="Student"
      sphere="Design"
      level="Junior"
      tasksDone={5}
      tasksCreated={0}
      students={0}
    />
    <AddSphereCard>Add new knowledge sphere</AddSphereCard>
    <AddSphereCard own>Create your own knowledge sphere</AddSphereCard>
    <AddSphereCard>Choose knowledge sphere from the list</AddSphereCard>
  </Wrapper>
)).add("ProgressCard", () => {
  return (
    tasks.map(task => {
      const onePercent = (task.tasksCompleted + task.tasksToPromotion) / 100;
      const percentCompleted = Math.round(task.tasksCompleted / onePercent);
      return (
        <ProgressCard
          task={task}
          percent={percentCompleted}
          diametr={92}
          strokeWidth={5}
          actions={{}}
          small
        />
  );
}).add("SphereCard popup", () => {
  return <PopupWithState/>;
});

export default Cards;
