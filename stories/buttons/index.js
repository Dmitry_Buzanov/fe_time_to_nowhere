import {addDecorator, storiesOf} from "@storybook/react";
import CloseButton from '../../src/components/buttons/closeButton/index';
import DefaultButton from '../../src/components/buttons/defaultBtn/index';
import DownloadButton from '../../src/components/buttons/downloadBtn/index';
import InfinityButton from '../../src/components/buttons/infinityBtn/index';
import PlusButton from '../../src/components/buttons/plusBtn/index';
import PlusTagButton from '../../src/components/buttons/plusTagButton/index';
import RemoveButton from '../../src/components/buttons/removeBtn/index';
import TagButton from '../../src/components/buttons/tagBtn/index';
import React from "react";
import {GlobalStyle} from "../../src/styles/index.js"


const Buttons = storiesOf('Buttons', module)
    .add('CloseButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <CloseButton>CloseButton</CloseButton>
        </React.Fragment>
    ))
    .add('DefaultButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <DefaultButton>DefaultButton</DefaultButton>
        </React.Fragment>
    ))
    .add('DownloadButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <DownloadButton>DownloadButton</DownloadButton>
        </React.Fragment>
    ))
    .add('InfinityButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <InfinityButton>InfinityButton</InfinityButton>
        </React.Fragment>
    ))
    .add('PlusButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <PlusButton>PlusButton</PlusButton>
        </React.Fragment>
    ))
    .add('PlusTagButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <PlusTagButton>PlusTagButton</PlusTagButton>
        </React.Fragment>
    ))
    .add('RemoveButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <RemoveButton>RemoveButton</RemoveButton>
        </React.Fragment>
    ))
    .add('TagButton', () => (
        <React.Fragment>
            <GlobalStyle/>
            <TagButton>TagButton</TagButton>
        </React.Fragment>
    ));

export default Buttons