import React from 'react';
import { withBackgrounds } from '@storybook/addon-backgrounds';
import {addDecorator, storiesOf} from '@storybook/react';
import Buttons from "./buttons/index";
import Icons from "./icons/index";
import Diagrams from "./diagrams/index";
import Menus from "./menus/index";
import Popups from "./popups/index";
import SingWith from "./singWith/index";
import Forms from "./forms/index";
import Table from "./table/index";
import Cards from "./cards/index"

