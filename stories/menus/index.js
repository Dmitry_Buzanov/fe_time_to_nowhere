import {storiesOf} from "@storybook/react";
import {MemoryRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "../../src/store";
import SideMenu from "../../src/components/menus/SideMenu/index";
import {BurgerButton} from "../../src/components/menus/Burger/index";
import React from "react";
import {GlobalStyle} from "../../src/styles";

const Menus = storiesOf('Menus', module)
    .addDecorator(story => (
        <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
    ))
    .add('SideMenu', () => (
        <Provider store={store}>
            <React.Fragment>
                <GlobalStyle/>
                <SideMenu/>
            </React.Fragment>
        </Provider>
    ))
    .add('Burger', () => (
        <BurgerButton/>
    ));

export default Menus