import {storiesOf} from "@storybook/react";
import Input from "../../src/components/forms/Input/index";
import React from "react";
import { Form } from 'react-final-form'


const Forms = storiesOf('Forms', module)
    .add('Input', () => (
      <Form onSubmit={()=>{}} render={() => <Input/>}/>
    ));

export default Forms