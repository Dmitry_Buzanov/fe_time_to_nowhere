import {storiesOf} from "@storybook/react";
import {Provider} from "react-redux";
import store from "../../src/store";
import SingWith from "../../src/components/signwith/components/index";
import React from "react";
import {GlobalStyle} from "../../src/styles";


const Sing = storiesOf('SingWith', module).add('SingWith', () => (
    <Provider store={store}>
        <React.Fragment>
            <GlobalStyle/>
            <SingWith actions={{}} location={{}}/>
        </React.Fragment>
    </Provider>
));

export default Sing