import {addDecorator, storiesOf} from "@storybook/react";
import Icon from '../../src/assets/Icon/index';
import * as i from "../../src/assets/Icon/constants";
import React from "react";
import "./index.css"

export default storiesOf('Icons', module)
    .add('BASKET', () => (
        <Icon type={i.BASKET}/>
    ))
    .add('SHARE', () => (
        <Icon type={i.SHARE}/>
    ))
    .add('PLUS', () => (
        <Icon type={i.PLUS}/>
    ))
    .add('DOWNLOAD', () => (
        <Icon type={i.DOWNLOAD}/>
    ))
    .add('SEARCH', () => (
        <Icon type={i.SEARCH}/>
    ))
    .add('HOME', () => (
        <Icon type={i.HOME}/>
    ))
    .add('LEARN', () => (
        <Icon type={i.LEARN}/>
    ))
    .add('SETTINGS', () => (
        <Icon type={i.SETTINGS}/>
    ))
    .add('LOGOUT', () => (
        <Icon type={i.LOGOUT}/>
    ))
    .add('LOGO', () => (
        <Icon type={i.LOGO}/>
    ))
    .add('PENCIL', () => (
        <Icon type={i.PENCIL}/>
    ))
    .add('SORT', () => (
        <Icon type={i.SORT}/>
    ));