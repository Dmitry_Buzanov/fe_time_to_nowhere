import React from "react";
import {storiesOf} from "@storybook/react";
import Popup from "../../src/components/popup/index";
import Button from "../../src/components/buttons/defaultBtn/index"
import {GlobalStyle} from "../../src/styles";

const PopupWithState = class extends React.Component {
    state = {
        isOpened: true
    };

    clickHandler = () => {
        const {isOpened} = this.state;
        this.setState({
            isOpened: !isOpened
        });
    };

    render() {
        return (
            <React.Fragment>
                <GlobalStyle/>
                <div>
                    {this.state.isOpened ? <Popup onClick={this.clickHandler}/> :
                        <Button onClick={this.clickHandler}>Открыть popup</Button>}
                </div>
            </React.Fragment>
        );
    }
};

const Popups = storiesOf("Popups", module).add("Popup", () => (
        <PopupWithState/>
    )
);

export default Popups;