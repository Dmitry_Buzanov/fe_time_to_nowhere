import { storiesOf } from "@storybook/react";
import Diagram from "../../src/components/diagram/components/index";
import React, { Fragment } from "react";
import { compose, withState } from "recompose";

const ComponentWithState = (
  stateName,
  stateUpdaterName,
  initialState,
  component
) => {
  return compose(withState(stateName, stateUpdaterName, initialState))(
    component
  );
};

const PercentDiagram = ComponentWithState(
  "value",
  "addPercent",
  10,
  ({ value, addPercent }) => {
    return (
      <Fragment>
        <Diagram
          actions={{
            addPercent
          }}
          percent={value}
          percents={4}
          diametr={200}
          strokeWidth={10}
        />
      </Fragment>
    );
  }
);
const DateDiagram = ComponentWithState(
  "value",
  "addDay",
  1,
  ({ value, addDay }) => {
    return (
      <React.Fragment>
        <Diagram
          actions={{
            addDay
          }}
          data={{}}
          day={value}
          days={0}
          diametr={200}
          strokeWidth={10}
        />
      </React.Fragment>
    );
  });

const Diagrams = storiesOf("Diagrams", module).add("Diagram", () => (
  <Fragment>
    <PercentDiagram />
    <DateDiagram />
  </Fragment>
));

export default Diagrams;
