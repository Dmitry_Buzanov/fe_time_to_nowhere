import { storiesOf } from "@storybook/react";
import React from "react";
import { GlobalStyle } from "../../src/styles";
import Table from "../../src/components/table/HOC/index";
import store from "../../src/store";
import { Provider } from "react-redux";

const table = storiesOf('Tables', module)
  .add('Table', () => (
    <React.Fragment>
      <GlobalStyle/>
      <Provider store={store}>
      <Table/>
      </Provider>
    </React.Fragment>
  ))

export default table